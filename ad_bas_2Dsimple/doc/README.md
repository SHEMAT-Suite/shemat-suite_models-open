# Testmodel 2Dsimple for inverse simulation with property moduel bas

This model is used as illustrative example for inverse simulation in the SHEMAT-Suite release publication.

* 2d model (x-z) with 150 x 1 x 75 cells, 2 m discretization
* Air dummy unit for filling the model domain above land surface
* eight subsurface rock layers with rather low permeability and varying thermal conductivity
* topographic flow gradient from left to right: 150 m - 126 m
* surface temperature: 11° C
* basal heat flow: 0.06 $W/m^2$
* Inverse Problem:
  Estimate thermal conductivity of units 4 to 9 from temperature log in the model center.
