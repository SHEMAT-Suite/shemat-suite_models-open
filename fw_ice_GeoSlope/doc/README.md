# Model Info: 
Simple 2D models wich show, that the ice property module is designed for low tempearatures.

Original input file: `GeoSlope_original`

## Compilation on RWTH Cluster:
```
CMakeCache_fw_gfortran_ice_none_head_master.txt
module_fw_gfortran_ice_none_head_master.inc
version_fw_gfortran_ice_none_head_master.inc
```

## Execution:
```
./shem_fw_gfortran_ice_none_head_master.x
```
