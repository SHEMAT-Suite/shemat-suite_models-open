# Model Info
Testmodel solving the one-dimensional Stefan problem (shorter modelled time to save computing time). The shorter model is in the main repository, the longer model can be found in the `long_time` repository.

## Information for short and long model
* Starting condition: every cell contains only ice at T=-3 degree Celsius

* Boundary condition: temperature of +30 degree Celsius at the left
                      no heat flux at all other boundaries

* This leads to melting of the ice from the left, no water circulation is assumed, only heat fluxes.

* Model dimensions: 900 cells with grid size of 0.1 meter in every direction:
        100 cells in x-direction (to let the ice melt in a nearly infinite half space) (models 10 meters)
        3 cells in y-direction (numerical reasons)
        3 cells in z-direction (numerical reasons)

## Information only for short model
* Time of transient calculation:
        time step size: 180 seconds (Neumann condition fullfilled)
        start time: 0 seconds
        end time: approximately 7 hours

### result:
        - the first cell has thawed, the second has thawed partially
        - the temperature field in the water is linear
        - the ice next to the area of phase change has been heated

## Information only for long model
* Time of transient calculation:
        time step size: 180 seconds (Neumann condition fullfilled)
        start time: 0 seconds
        end time: 3 months

### result:
        - nearly 80 cm have thawed
        - the temperature field in the water is linear
        - the ice next to the area of phase change has been heated

# Compilation Information
```
module_fw_ifort_ice_none_head_master_quick.inc
CMakeCache_fw_ifort_ice_none_head_HEAD_quick.txt
version_fw_ifort_ice_none_head_master_quick.inc
out_compilation.txt
```

# Comparison
The original script was written in matlab, it has been translated to python. The matlab scripts can be found in `semi-analytical_Matlab`.

The Stefan problem is solved semi-analytically.

The two-phase-solution means that the ice can be colder than its melting temperature.

Source: Bouciguez, A. C., Lozano, R. F., and Lara, M. A., ABOUT THE EXACT SOLUTION IN TWO PHASE-STEFAN PROBLEM, Engenharia Termica (Thermal Engineering), Vol. 6, N 02, p.70-75, 2007


The differences between the numerical and the semi-analytical solution are due to the following: In the semi-analytical calculation, the ice melts at 0ºC. In the numerical calculation, the ice melts between the solidus temperature of -2ºC and the liquidus temperature of 0ºC. At the solidus temperature, most water is frozen and at the liquidus temperature, all ice is molten (according to the function theta which rises from 0 to 1 and which is multiplied with the water content, the ice content is the remaining pore space without the water content).

This numerical calculation is useful for groundwater and permafrost, but it is not represented by the semi-analytical solution. Changing the solidus temperature or the liquidus temperature would lead to a different solution. Note in this case, that every point has to be once between these two temperatures to model the phase change including the latent heat. Otherwise, only a sudden material change occurs not representing a phase change.

# Test-Suite
Only the short time model is implemented in the test-suite. However a output of the test-suite with the `long_time` model can be found here: `fw_ice_stefanproblem/long_time/doc/out_testSuite_long`.
