%% Stefanproblem_Dirichlet_plot

% Show results

t_plot = t/t_unit; % show results in normal time unit and not usually in seconds, the unit in which the calculation has to be

figure()
imagesc(t_plot,z,T)
title('Two Phase Stefan Problem 1D','fontsize',16)
xlabel(['Time [' t_unit_name ']'],'fontsize',12)
ylabel('Depth [m]','fontsize',12)
grid on;
colorbar();
colormap('jet');
annotation('textbox',...
    [0.848720800889878 0.927469105526514 0.0444938820912125 0.055666003976143],...
    'String',{'T[\circC]'},...
    'FitBoxToText','off',...
    'LineStyle','none');

figure()
plot(t_plot,mass_melted)
title('Mass Melted per Area','fontsize',16)
xlabel(['Time [' t_unit_name ']'],'fontsize',12)
ylabel('Mass per area [kg/m^2]','fontsize',12)
grid on;

figure()
plot(t_plot,z_m)
title('Depth of Melting Point (Two Phase Stefan Problem)','fontsize',16)
xlabel(['Time [' t_unit_name ']'],'fontsize',12)
ylabel('Depth [m]','fontsize',12)
grid on;

figure()
plot(z,T(:,end))
title(['Temperature Profile after ',num2str(t_plot(end)),' ',t_unit_name, ' (Two Phase Stefan Problem)'],'fontsize',16)
xlabel('Depth [m]','fontsize',12)
ylabel('Temperature [\circC]','fontsize',12)
grid on;

