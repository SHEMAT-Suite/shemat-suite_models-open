function [ lambda_St ] = Stefanproblem_Stefan_number_and_lambda( c_water,c_ice,ny,T_bc,T_0,T_m,L )
% [ lambda_St ] = Stefanproblem_Stefan_number_and_lambda( c_water,c_ice,ny,T_bc,T_0,T_m,L )
% % Calculate Stefan number St = (c * (T_bc - T_m))/L (for water (c_water) and ice (c_ice))
% % lambda_St = x_m/(sqrt(t)*2*k/(rho*c)) (Ansatz)
% % lambda_St = fzero(@(lambda_St) -lambda_St * sqrt(pi) + (St_water /( exp(lambda_St*lambda_St) / erf(lambda_St)) - St_ice /(ny * exp(ny * ny * lambda_St*lambda_St) * erf(ny * lambda_St)) ),1);


% Stefan number St and helping number lambda
St_water = (c_water * (T_bc - T_m))/L; % dimensionless Stefan number
St_ice   = (c_ice   * (T_m  - T_0))/L; % dimensionless Stefan number

% find root which determines helping variable lambda
lambda_St = fzero(@(lambda_St) ...
        lambda_St * sqrt(pi) ...
        - St_water * exp(-lambda_St*lambda_St) / erf(lambda_St) ...
        + St_ice / (ny * exp(ny^2 * lambda_St^2) * erfc(ny*lambda_St)),1);
    
end

