The Stefan problem is solved semi-analytically

The two-phase-solution means that the ice can be colder than its melting temperature.

Source: Bouciguez, A. C., Lozano, R. F., and Lara, M. A., ABOUT THE EXACT SOLUTION IN TWO PHASE-STEFAN PROBLEM, Engenharia Termica (Thermal Engineering), Vol. 6, N 02, p.70-75, 2007

Please let run the script Stefanproblem_Dirichlet_main.m
It will open the other files as needed.