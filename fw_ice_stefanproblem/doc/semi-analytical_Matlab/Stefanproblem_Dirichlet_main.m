%% Stefan problem 1D - the two phase-Stefan problem solved semi-analytically
% 
% Given the latent heat and specific heat capacity, the Stefan problem is
% solved in one dimension, this means, it is calculated how fast ice melts
% and how much ice melts.
% The ice is heated until the melting temperature. There, it melts and
% then, the water is heated (from one side where a Dirichlet boudnary 
% condition is present).

% Source: Bouciguez, A. C., Lozano, R. F., and Lara, M. A., ABOUT THE EXACT
% SOLUTION IN TWO PHASE-STEFAN PROBLEM, Engenharia Termica (Thermal
% Engineering), Vol. 6, N 02, p.70-75, 2007


clear all;
close all;
clc;

% Set variables
Stefanproblem_Dirichlet_variables

% Calculate Stefan number and with that lambda
ny = sqrt(alpha_water / alpha_ice);
lambda_St = Stefanproblem_Stefan_number_and_lambda(c_water,c_ice,ny,T_bc,T_0,T_m,L);

% Calculate temperature
T_water = zeros(length(z),length(t));
T_ice = T_water;
T_water = zeros(length(z),length(t));
for i_t = 1:1:length(t)
    T_water(:,i_t) = T_bc - (T_bc-T_m)*( erf(z/(2*sqrt(alpha_water * t(i_t)))) ./ erf(    lambda_St) );
    T_ice(:,i_t)   = T_0  - (T_0-T_m)*( erfc(z/(2*sqrt(alpha_ice   * t(i_t)))) ./ erfc(ny*lambda_St) );
end

% check if the two temperature fields fit together
T = zeros(size(T_water));
T(T_water >=T_m) = T_water(T_water >=T_m);
T(T_ice <T_m) = T_ice(T_ice <T_m);
if (max(max((T_water >=T_m) & (T_ice <T_m)))) > 0
    ['ERROR in T_{water} and T_{ice}']
end

z_m = 2 * lambda_St * sqrt(alpha_water * t); % melting depth
mass_melted = z_m * rho_ice; % calculate mass per area (cell number times volume times density per area)

%% show results
Stefanproblem_Dirichlet_plot

