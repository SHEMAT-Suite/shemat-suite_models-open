%% Set Variables for Stefan problem

% TIME
t_unit = 60*60*24; % time unit to calculate time in seconds
t_unit_name = 'd'; % time unit in legends of plots
dt = 0.1; % time interval / step size in chosen unit
t_end = 3*30; % length of time in chosen unit
t = dt:dt:t_end; % one row, can't begin with zero due to calculation of T_Stefan
t = t * t_unit; % calculation has to be in seconds

% DEPTH
dz = 0.01; % depth interval in meters
zmax = 10; % maximal depth in meters
z = (0:dz:zmax)'; % depth (one column)
z_m = zeros(size(t))'; % depth of interface between water and ice

% TEMPERATURE BOUNDARY CONDITION
T_bc = 30; % Temperature in maximal depth of model (Dirichlet boundary condition)
T_0 = -3; % Start temperature of the whole ice
T_m = 0; % Melting temperature of the ice

% THERMAL PROPERTIES OF WATER at 0.01?C (Lide, 2010, ?Thermophysical Properties of Water and Steam?; Lide, 2010, ?Properties of Ice and Supercooled Water?)
lambda_water = 0.5611; % thermal conductivity in W / (m K)
c_water = 4219.4; % Specific heat capacity of water in J/(kg K)
rho_water = 999.84; % density of water in kg/m^3
alpha_water = lambda_water / (rho_water * c_water);
L = 333641.9; % Latent heat in J / kg

% THERMAL PROPERTIES OF ICE at 0?C (Lide, 2010, ?Thermophysical Properties of Water and Steam?; Lide, 2010, ?Properties of Ice and Supercooled Water?)
rho_ice = 916.7; % density of ice in kg/m^3
c_ice = 2110; % specific heat capacity in J/ (kg K)
lambda_ice = 2.14; % thermal conductivity in W / (m K)
alpha_ice = lambda_ice / (rho_ice * c_ice);
% L_ice = 0; % Latent heat in J / kg

% INITIALIZE FIELDS
T_Stefan = zeros(length(z),length(t)); % initialize temperature field
mass_melted = zeros(1,length(t)); % initialize the mass of the resulting water
mass_index = ones(length(z),1); % helping variable to say in which cells the ice melted
