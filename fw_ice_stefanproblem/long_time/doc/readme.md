# Model Info:
See `fw_ice_stefanproblem/doc/readme.md`.

# Compilation Information
```
version_fw_ifort_ice_none_head_master_quick.inc
module_fw_ifort_ice_none_head_master_quick.inc
CMakeCache_fw_ifort_ice_none_head_master_quick.txt
```

# Test-Suite
This model is not in the test-suite. However the output of a test-suite run, from 25.05.2020 with the `long_time` model can be found in `out_testSuite_long`.
