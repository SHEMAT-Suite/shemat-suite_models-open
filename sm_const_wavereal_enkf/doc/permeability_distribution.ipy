import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# Run sm_const_wavereal_enkf with # simulate 1000 and # nrobs_int 10 and use the results
try:
    import pskf.tools.plot.plotfunctions as pf
except ImportError:
    raise ImportError(
        'No module named pskf.tools.plot.plotfunctions. \n\n' + 'Solution: ' +
        'Download pyshemkf from https://github.com/jjokella/pyshemkf and add' +
        ' the path /<path-to-pyshemkf>/pyshemkf/site-packages to the' +
        ' PYTHONPATH.')


hdf_f_dir = os.environ[
    'HOME'] + '/SHEMAT-Suite_Models/sm_const_wavereal_enkf/samples_output/'

iloc = 22
jloc = 16

kz_prior = np.zeros(1000)
kz_post = np.zeros(1000)

for i in range(1,1001):
    hdf_f_name = 'WAVEREAL_E0_init_'+str(i)+'.h5'
    kz_prior[i-1] = np.log10(pf.my_hdf(hdf_f_dir+ hdf_f_name, 'kz')[iloc-1, jloc-1, 0])

for i in range(1,1001):
    hdf_f_name = 'WAVEREAL_E1_'+str(i)+'.h5'
    kz_post[i-1] = np.log10(pf.my_hdf(hdf_f_dir+ hdf_f_name, 'kz')[iloc-1, jloc-1, 0])

plt.vlines(np.mean(kz_prior),0.0,1.0, colors="lightcoral")
plt.vlines(np.mean(kz_post),0.0,1.0, colors="cornflowerblue")
plt.vlines(np.mean(-11.76),0.0,1.0, colors="black")
plt.hist([kz_prior, kz_post], 20, color=["lightcoral","cornflowerblue"], normed=True)
plt.legend(["Prior Mean","Updated Mean", "True mean", "Prior Distribution", "Update Distribution"])
plt.xlabel(r"$\log_{10}(K/\mathrm{m}^{2})$")
plt.ylabel("Probability density")
plt.title("Log-Permeability distributions at cell (22, 16)")
plt.show()
