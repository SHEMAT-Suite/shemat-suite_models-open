# Model Info
Testmodel for verification and comparison of head-based and pressure-based computation.
This is the head-based model.
``fw_pres_bas_Head_pres`` is the pressure-based model.

* Simple quasi 1D model (x-z) with coupled heat and fluid flow for verifying and comparing the head-based and pressure-based solution of the flow equation with
temperature and pressure dependent fluid properties:
This model solves the flow equation (Darcy) for hydraulic constant density reference potential (head) h0
The other model solves the flow equation (Darcy) for pressure P

* Model: 500 m (x) x 500 m (z), delx= delz= 10 m

# Compare
The compare scripts also use the results of ``fw_pres_bas_Head_Pres``.

# Compilation Information
```
module_fw64int_bas_head_master.inc
Makefile_fw64int_bas_head_master.flags
version_fw64int_bas_head_master.inc
```

# Execution
```
shem_fw64int_bas_head_master.x
```
