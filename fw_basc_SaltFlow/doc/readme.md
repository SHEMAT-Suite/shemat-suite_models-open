# Model Info: 
Simple 2D transient modelks for flow and salt transport which show the ability of the property module basc to simulate density driven flow of aqueous NaCl solutions. 

Note: SaltFlow_small is a smaller version of the model, wich runs in about 1 second
on one core.

# Compilation Info:
```
CMakeCache_fw_gfortran_basc_none_head_master.txt
module_fw_gfortran_basc_none_head_master.inc
version_fw_gfortran_basc_none_head_master.inc
```

# Execution:
./shem_fw_gfortran_basc_none_head_master.x
