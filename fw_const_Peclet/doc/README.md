# Model Info: 1D model for temperature- and massflow #
vertical temperature profile influenced by vertical flow

# Compilation on Ubuntu Laptop:
```
./compilequick_run.sh
```

# Compilation Information
```
module_fw_gfortran_const_none_head_master.inc
CMakeCache_fw_gfortran_const_none_head_master.txt
version_fw_gfortran_const_none_head_master.inc
```
# Execution:
```
./shem_fw_gfortran_const_none_head_master.x
```

# Comparison
`bredehoeft_analytical_solution.png` shows the analytical and numerical solution.
`resid_bredehoeft-SHEMAT.png` shows the realtive difference of the analytical and numerical solution.
The plots have been created with `analytical_peclet.ipy`.

The analytical solution was taken from `Bredehoeft1965.pdf`.

The original matlab script can be found in `W:\GGE_Public\SHEMAT\SHEMAT-Suite\Testmodelle\shemat-suite_testmodelle_esim\test1D_Coupled_Bredehoeft\matlab`, in there also the derivations are plotted.
