# Model 2D Fancy Geology - Forced and Free Convection #

This is a 2D benchmark model for pressure-based computation of forced- and free convection with a more complex geological structure. In comparison, models in `fw_bas_2Dfancy`
are an example for the head-based simulation of the same models. In contrast to the free convection simulation, there is a varying pressure gradient at the top model boundary and unit permeabilities are higher, causing advection and advective heat transport. 

Both have 220x1x119 cells with 50x50x50 m discretization. 

Workflow for pressure-based modeling with SHEMAT-Suite:
1. Hydrostatic initial conditions (file `forced_init`): Simulate initial hydrostatic pressure field for local thermal conditions.
2. Thermal initial conditions (file `forced_init_thermal`: Simulate initial thermal conditions for realistic model structure with initial conditions from *1*.
3. Flow and heat transport model: Coupled simulation with realistic model structure and initial conditions from *1* (for pressure) and *2* (for temperature). Note that pressure needs a noflow boundary condition at the base.

In this repository these steps are allready implemented in shemade.job.

total cpu time:    0:21.84 min

date: August 2019

contact person: Johanna Bruckmann

## hdf5 version ##

The executables of this model were compiled with `hdf5/1.8.21`. This is at the moment not the default version, so you have to load it manually using.
During the merge of both models in one repository `hdf5/1.10.4` has been used. No significant differences have been seen.

```
module load hdf5/1.8.21
```

or, if you already have loaded `hdf5`

```
module switch hdf5 hdf5/1.8.21
```

# Compilation information
```
module_fw_ifort_bas_none_pres_master_quick.inc
CMakeCache_fw_ifort_bas_none_pres_master_quick.txt
version_fw_ifort_bas_none_pres_master_quick.inc
```

# Execution
```
./shem_fw_ifort_bas_none_pres_master_quick.x > out.txt
```
