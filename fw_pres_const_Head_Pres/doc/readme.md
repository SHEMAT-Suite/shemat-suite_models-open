# Model Info
Testmodel for verification and comparison of head-based and pressure-based computation
This is the pressure based model.
`fw_const_Head_Pres` is the head based model.

* Simple quasi 1D model (x-z) with coupled heat and fluid flow for verifying and comparing the head-based and pressure-based solution of the flow equation with
constant fluid properties:
This model solves the flow equation (Darcy) for pressure P
The other model solves the flow equation (Darcy) for hydraulic constant density reference potential (head) h0

* In addition, this testmodel gives an easy example for a pressure-based input file

* Model: 500 m (x) x 500 m (z), delx= delz= 10 m

# Compare
The compare scripts can be found in `fw_const_Head_Pres`, these scripts also use the results of this modell. The three plots, wich can be also found in this doc repository are created with these scripts. 

# Compilation on CLAIX-18:
make pres fw hdf PROP=const COMPTYPE=ling64 HDF5_LIB=$HDF5_ROOT/lib HDF5_MOD=$HDF5_ROOT/include -j 8 

# Compilation Information
```
module_fw64int_const_pres_master.inc
Makefile_fw64int_const_pres_master.flags
version_fw64int_const_pres_master.inc
```

# Execution
```
./shem_fw64int_const_pres_master.x
```
