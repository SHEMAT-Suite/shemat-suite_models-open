# Model Info
Testmodel for verification and comparison of head-based and pressure-based computation.
This is the pres-based model.
``fw_bas_Head_Pres`` is the head-based model.

* Simple quasi 1D model (x-z) with coupled heat and fluid flow for verifying and comparing the head-based and pressure-based solution of the flow equation with
temperature and pressure dependent fluid properties:
This model solves the flow equation (Darcy) for pressure P
The other model solves the flow equation (Darcy) for hydraulic constant density reference potential (head) h0
  
* In addition, this testmodel gives an easy example for a pressure-based input file

* Model: 500 m (x) x 500 m (z), delx= delz= 10 m

# Compare
comparescripts can be found in ``/fw_bas_Head_Pres/doc``. These scripts also use the results from this model.

# Compilation Information
```
Makefile_fw64int_bas_pres_master.flags
module_fw64int_bas_pres_master.inc
version_fw64int_bas_pres_master.inc
```

# Execution
```
./shem_fw64int_bas_pres_master.x
```
