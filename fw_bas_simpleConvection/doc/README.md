# Model Info:
Simple 2D and 3D free Convection modells, steady-state and transient.
The transient solutions don't reach the steady state.

The Energy inflow in the 2D Model was chosen so that the fraction of Energy inflow over Model Volume equals the 3D Model.
The 3D Model does not have a straight line of energy inflow at the bottom, that means the 2D Model does not describe the same situation.

**2D Model**

Volume: 10⁵ m³

Heat flow: 0,025 W/m² in 2 cells (each 100 m²) --> 5 W

**3D Model**

Volume: 10⁶ m³

Heat flow: 0,05 W/m² in 10 cells (each 100 m²) --> 50 W

**Problems with transient computation**: Changing nummber of timesteps
changes the result, fewer timesteps leads to a result closer to steady
state, a lot more timesteps lead to a result where every cell is 10
degrees.

# Compilation documantation:

```
version_fw_ifort_bas_none_head_master-all_quick.inc
module_fw_ifort_bas_none_head_master-all_quick.inc
CMakeCache_fw_ifort_bas_none_head_master-all_quick.txt
```

## Execution: ##

``` shell
./shem_fw_ifort_bas_none_head_master-all_quick.x > out.txt
```
