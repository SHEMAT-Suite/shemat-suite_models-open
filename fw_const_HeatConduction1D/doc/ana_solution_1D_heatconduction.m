%% Analytic solution for the 1D, transient heat transport problem with fixed temperatures at the boundaries

clear;
close all;
    
%%
nmax =  20;

iniTemp = 25;           %initial temp
T1 = 14;                %time start
T2 = 25;                %time end
L = 20.02;              %length of model (L)
alpha = .25;            %thermal diffusivity

f  = iniTemp;


%% symbolische Berechnung
syms n integer
syms x t positive

% symbolisches Integral
uE = T1 + (T2 - T1)/L * x;
bfun = (f-uE) * sin(n*pi*x/L);
bint = 2/L * int(bfun,x,[0 L]);

% Summe mit symbolischem Integral
sfun = bint'*sin(n*pi*x/L)*exp(-alpha*t*(n*pi/L)^2);

% Summe mit syms
S = symsum(sfun, n, 1, nmax);
SE = uE + S;    % Ergebnis der symbolischen Berechnung = SE(x,t)

%% Berechnung der Werte f�r spezif. x und t
ts = 0 : 60 : (10*60);
x_ana = 0 : 1 : L;

u = zeros(length(x_ana),length(ts));

    tic
for i = 1:length(x_ana)
    x = x_ana(i);
    for j = 1:length(ts)
        t = ts(j);
        u(i,j) = eval(SE);
        
                if toc > 10
                    fprintf('Iteration x %d/%d\n',i,length(x_ana))
                    fprintf('Iteration time %d/%d\n',j,length(ts))
                    tic
                end
    end
    
end


%% Plotten
figure()
plot(x_ana,u(:,1),'-o', x_ana,u(:,3),'-o',x_ana,u(:,10),'-o', x_ana,u(:,end),'-o')
xlabel('length(m)'); ylabel('T (�C)');
legend('time step 1', 'time step 3', 'time step 10', 'last time step')
xlim([0,L])

%export image
if exist('print_it', 'var')
    set(gcf, 'Color', 'w');
    fig = gcf;
    exp_str = strcat(exp_folder,fname,'_fig1 x_diff_timesteps');  %Exportname zusammenbauen
    I_print(exp_str,fig);
end

%%
figure()
plot(ts,u(3,:),'-o', ts,u(5,:))
xlabel('Zeit(s)'); ylabel('T (�C)');
set(gca, 'XScale', 'log');
legend('cp 1', 'cp2 3')

%export image
if exist('print_it', 'var')
    set(gcf, 'Color', 'w');
    fig = gcf;
    exp_str = strcat(exp_folder,fname,'_fig2 transient');  %Exportname zusammenbauen
    I_print(exp_str,fig);
end
%%
figure()
image(ts',x_ana',u,'CDataMapping','scaled'); axis xy;
colorbar
title('Temperature Evolution')
ylabel('length(m)'); xlabel('time (s)')

%export image
if exist('print_it', 'var')
    set(gcf, 'Color', 'w');
    fig = gcf;
    exp_str = strcat(exp_folder,fname,'_fig3 surf');  %Exportname zusammenbauen
    I_print(exp_str,fig);
end
%%
save('M_ana_lsg_sym_norbert_nmax_20','ts','x_ana','u','nmax');




