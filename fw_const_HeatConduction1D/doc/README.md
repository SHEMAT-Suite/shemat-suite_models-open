# Model Info: Heat Conduction 1D

transient heat conduction models 

Length	20,02 m	
initial temperature 25 °C
boundary condition left 14 °C	
boundary condition right 25 °C

Three models are in this directory, but at the moment only `heat_conduction` is set active in `shemade.job`:
`heat_conduction`: different grid and timesteps
`green_1d_transient_G6-h`: different grid, but same time-steps compared to `green_1d_transient_G3-h`
`green_1d_transient_G3-h`: different grid, but same time-steps compared to `green_1d_transient_G6-h`

# Compilation Information
```
module_fw_gfortran_const_none_head_master_quick.inc
version_fw_gfortran_const_none_head_master_quick.inc
CMakeCache_fw_gfortran_const_none_head_master_quick.txt
```

# Execution
```
./shem_fw_gfortran_const_none_head_master_quick.x
```


# Comparison
`ana_solution_1D_heatconduction.ipy` creates
`ana_solution_1D_heatconduction.npy` which is used in the comparison:
`shem_ana_comparison_1D_heat_conduction.ipy`.

`ana_solution_1D_heatconduction.m` creates `M_ana_lsg_sym_norbert_nmax_20.mat`
