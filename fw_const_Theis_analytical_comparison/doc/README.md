# fw_const_Theis_and_Theis_centered_analytical_comparison #

Theis comparison with analytical formula from SHEMAT book Clauser2003.

The main problem is a numerical difference between the pumping rate
from the analytical formula:

```
Q = 0.004 m3/s
```
and the SHEMAT-Suite input for the Neumann boundary condition

```
# head bcn, records=1, direction=0
1 1 1 -0.001d0 0
```
## Python scripts ##

The Python scripts use functions computing the approximate Theis
solution from the Python module `pskf`. If you do not have `pskf`:
Download `pyshemkf` from https://github.com/jjokella/pyshemkf and add
the path `/<path-to-pyshemkf>/pyshemkf/site-packages` to the
`PYTHONPATH`.

`theis_analytical.ipy`: Compares the Theis-analytical solution the
SHEMAT-Suite result loaded from the vtk files in `/result/`. The
figures are saved in `/doc/pdfs/`.

`theis_monitoring_points.ipy`: Compares the Theis-analytical solution
at one cell over time the SHEMAT-Suite result loaded from the monitor
files in `/result/`. The figures are saved in `/doc/pdfs/`.

At the moment these scripts only analyse THEIS_centered.

## Compilation ##

The files 
```
CMakeCache_fw_ifort_const_none_head_master_quick.txt
module_fw_ifort_const_none_head_master_quick.inc
version_fw_ifort_const_none_head_master_quick.inc
```
contain the compilation information.


## Execution ##

``` shell
./shem_fw_ifort_const_none_head_master_quick.x > out.txt
```

total cpu time:    0: 2.65 min

## Citations ##

Clauser, C., Numerical simulation of reactive flow in hot aquifers:
shemat and processing shemat (2003), : Springer-Verlag Berlin
Heidelberg New York.

