# Model Info: Elder Problem test case, from Denise #

benchmark_elder_basc is a steady state model of the Elder Problem
benchmark_elder_basc_transient is a transient model, only with the
first time step(s), of the Elder Problem. Both are from Denise Degen.
Degen's bachelor thesis (for more information, see the GGE-Wiki:
https://ggewiki.eonerc.rwth-aachen.de/index.php/Coupling_between_tracer_transport_and_groundwater_flow)

## Compilation on RWTH Cluster: ##

Compilation documentation:
```
CMakeCache_fw_ifort_basc_none_head_master_quick.txt
module_fw_ifort_basc_none_head_master_quick.inc
version_fw_ifort_basc_none_head_master_quick.inc
```


## Execution: ##

``` shell
./shem_fw_gfortran_basc_none_head_master.x
```

# Background information #

In the course of two bachelor theses the coupling between tracer
transport and groundwater flow have been tested using the Elder
problem and the Henry problem.

The bachelortheses can be found here: `W:\GGE_Public\SHEMAT`

Elder problem (in German)
=========================

Das von J. W. Elder (1966) entwickelte Elder-Problem basiert auf
Laborexperimenten und numerischen Kalkulationen. Es befasst sich mit
der freien Konvektion in einem vertikalen Spalt eines porösen
Mediums. Bei diesem Problem entsteht die Strömung durch einen
Dichtekontrast, der bei Elder thermisch hervorgerufen wurde. Nach
Modifikationen durch Diersch (1981) und Voss und Souza (1987) wird der
Dichtekontrast nun durch einen Konzentrationsunterschied erzeugt. Der
Benchmark ist rein hypothetisch, da der in der Natur wichtige Prozess
der Dispersion keine Beachtung findet (Diersch und Kolditz 2002). Die
Dimensionen und Randbedingungen des Problems sind in Abbildung 1
skizziert.

![alt text](./SCHEMATElderProblem_1.png)

Das Elder-Problem_besitzt keine eindeutige Lösung. Außerdem weist es
die Eigenheit auf, dass sich durch Änderungen in der räumlichen
Diskretisierung auch das Strömungsverhalten ändert. So tritt in der
Literatur oberhalb von 4400 Knoten ein zentraler Auftrieb und
unterhalb ein zentraler Abstrom auf. Die Grenze für die Änderung im
Strömungsverhalten ist abhängig von den verwendeten numerischen
Verfahren (Diersch und Kolditz 2002). Nach Johannsen (2002) existieren
im klassischen Bereich des Elder-Problems, also bei einer Rayleighzahl
von 400, drei stabile stationäre Lösungen. In SHEMAT-Suite sind die
gängigen Literaturlösungen und die Ergebnisse von Johannsen (2002)
simuliert worden. Es erfolgten daher u.a. Simulationen für
unterschiedliche räumliche Diskretisierungen sowie unterschiedliche
Rayleighzahlen. Dabei wurden die in Tabelle 1 aufgeführten Parameter
gewählt. Die Simulationen fanden für zwei Modelle statt. Beim ersten
Modell ist nur die Abhängigkeit der Dichte von der Salzkonzentration
betrachtet worden, beim zweiten Modell dahingegen wurde die
Abhängigkeit sämtlicher Fluideigenschaften von der Salzkonzentration
berücksichtigt.

![alt text](./SCHEMATElderProblem_2.png)

![alt text](./SCHEMATElderProblem_3.png)

Dabei stellte sich heraus, dass SHEAMT-Suite andere Ergebnisse
abbildet als in den bisher publizierten Werken. So entspricht die
Lösung für ein grobe räumliche Auflösung der einer feinen räumlichen
Auflösung in SHEMAT-Suite und umgekehrt. Auf Grund der Simulationen
nach Johannsen (2002) konnte jedoch festgestellt werden, dass es sich
bei den mit SHEMAT-Suite produzierten Lösungen ebenfalls um Lösungen
des Elder-Problems handelt. Quantitative Vergleiche mit den Daten aus
Guo und Langevin (2002), Johannsen (2002) und Clauser (2003) sind in
den Abbildungen 2 - 5 dargestellt. Die Abweichungen (Abbildung 2 und
3) kommen durch unterschiedliche numerische Verfahren zustande. In den
Abbildungen 4 und 5 ist zu beachten, dass Johannsen (2002) einen
stationären Ansatz wählte im Gegensatz zum instationären dieser
Arbeit.  Durch eine Verlängerung der Simulationszeit würden sich die
Ergebnisse weiter annähern.

![alt text](./SCHEMATElderProblem_4.png)

![alt text](./SCHEMATElderProblem_5.png)

![alt text](./SCHEMATElderProblem_6.png)

![alt text](./SCHEMATElderProblem_7.png)

Desweiteren wurde festgestellt, dass die in der Literatur verwendeten
Modelle vermutlich nur die Dichte in Abhängigkeit der
Salzkonzentration betrachten. Deshalb wurde der quantitative Vergleich
lediglich für das erste Modell durchgeführt. Für weitere Informationen
wird auf Degen (2014) verwiesen.

![alt text](./SCHEMATElderProblem_8.png)

![alt text](./SCHEMATElderProblem_9.png)

Please review this README and remove
this notification if everything is up-to-date. 

