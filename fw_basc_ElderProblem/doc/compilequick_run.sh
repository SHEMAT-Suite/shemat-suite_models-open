#!/bin/zsh

# Compile SHEMAT-Suite executable with cmake and copy it to model_dir

#------------------------------------------------------------------
#-----------------------Variables ---------------------------------
#------------------------------------------------------------------
model_dir="/home/jke/SHEMAT-Suite_Models/fw_basc_ElderProblem"         # "${HOME}/SHEMAT-Suite_Models/fw_const_Example"
make_dir="/home/jke/SHEMAT-Suite"                     # "${HOME}/SHEMAT-Suite"

shem_type="fw"			# "sm", "fw"

mode="head"			# "head", "pres"

props="basc"
user="none"

compiler_name="gfortran"	       # "gfortran","ifort"

flags="-Domp=OFF -Dhdf=ON" # Flags: "-Domp=ON","-Dhdf=ON","-Ddetails=ON"

#Make-directory existence check
if [ ! -d ${make_dir} ]
then
    echo "   Makefile directory"
    echo ${make_dir}
    echo "   does not exist."
    exit 1
fi

# Go to make_dir
pushd ${make_dir}

# Module configuration (for debugging, remove 2> /dev/null)
module purge 2> /dev/null
module load DEVELOP 2> /dev/null
module load gcc 2> /dev/null
module load intelmpi 2> /dev/null
module load LIBRARIES 2> /dev/null
module load hdf5 2> /dev/null
module load cmake 2> /dev/null
module load LIBRARIES 2> /dev/null
module load LIBRARIES 2> /dev/null

# Get git branch name
git_branch="master"

# Check source code for changes
git diff --exit-code --quiet
if [ $? -ge 1 ];
then
    echo "   Unstaged changes in Git repository! Please remove."
    echo "   Branch: ${git_branch}"
    exit 1
fi

git diff --cached --exit-code --quiet
if [ $? -ge 1 ];
then
    echo "   Staged changes in Git repository! Please remove."
    echo "   Branch: ${git_branch}"
    exit 1
fi

if [ ${git_branch} = $(git rev-parse --abbrev-ref HEAD) ]
then
    echo "   SHEMAT-Suite repository in branch"
    echo ${git_branch}
else
    echo "   SHEMAT-Suite repository not yet in branch"
    echo ${git_branch}
    git checkout ${git_branch}
    # gmake dep
    echo "   SHEMAT-Suite repository in branch"
    echo ${git_branch}
fi

# # Generate dependency file if necessary
# if [ ! -e Makefile.dep ]
# then
#     gmake dep
# fi

#Clean make-directory
# gmake cleanall

# Remove old build directory if still exists
if [ -d build_${props} ]
then
    rm -r build_${props}
fi
#Clean make-directory
mkdir build_${props}
pushd build_${props}

FC=${compiler_name} cmake -DPROPS=${props} -DUSER=${user} -Dphys_base=${mode} ${flags} ..

# Catch configuration errors
if [ $? -ge 1 ];
then
    echo ""
    echo "compilequick.sh: Command:"
    echo "------------------------------------"
    echo "FC=${compiler_name} cmake -DPROPS=${props} -DUSER=${user} -Dphys_base=${mode} ${flags} .."
    echo "------------------------------------"
    echo "compilequick.sh: Compilation aborted"
    popd
    exit 1
fi

#Compilation command 
gmake ${shem_type} -j16

# Catch compilation errors
if [ $? -ge 1 ];
then
    echo ""
    echo "compilequick.sh: Command:"
    echo "------------------------------------"
    echo "gmake ${shem_type} -j16"
    echo "------------------------------------"
    echo "compilequick.sh: Compilation aborted"
    popd
    exit 1
fi

if [ ! ${compiler_name} = $(grep "CMAKE_Fortran_COMPILER:FILEPATH" CMakeCache.txt | grep -Eo "[a-zA-Z0-9]+$") ]
then
    echo ""
    echo "compilequick.sh:"
    echo "------------------------------------"
    echo "${compiler_name}"
    echo "not equal to"
    echo "$(grep "CMAKE_Fortran_COMPILER:FILEPATH" CMakeCache.txt | grep -Eo "[a-zA-Z0-9]+$") (compiler_name in CMakeCache.txt)"
    echo "------------------------------------"
    echo "compilequick.sh: Compilation aborted"
    popd
    exit 1
fi

#New executable suffix
new_exe_suffix="${shem_type}_${compiler_name}_${props}_${user}_${mode}_${git_branch}"

# Rename executable
rename shem_${shem_type}_${props}.x shem_${new_exe_suffix}.x shem_${shem_type}_${props}.x

# Copy executable
cp shem_${new_exe_suffix}.x ${model_dir}

# Copy CMakeCache.txt, version.inc
cp CMakeCache.txt ${model_dir}
pushd generated
cp version.inc ${model_dir}
popd

# Rename CMakeCache.txt, version.inc
pushd ${model_dir}
rename CMakeCache.txt CMakeCache_${new_exe_suffix}.txt CMakeCache.txt
rename version.inc version_${new_exe_suffix}.inc version.inc

# File with RWTH cluster module configuration
module list -t 2> module_${new_exe_suffix}.inc

# Clean SHEMAT-Suite git repository
popd
git clean -f

# Create and move tgz Backup
popd
# gmake tgz
# mv *.tgz ${model_dir}
rm -r build_${props}

# Echo paths
echo "--------------------------------------------------------"
echo "Compilation path:"
pwd
popd

echo
echo "Current path:"
pwd
echo
