# Skript to read and plot monitordata from monitor.dat files (SHEMAT)
# date: october 2019
# author: Dominique Knapp

import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from pandas import DataFrame, Series

# Define Path for File Loading
path1 = os.environ[
    "HOME"] + '/SHEMAT-Suite_Models/fw_bas_doublet_model/result/Doublette_testmodel_monitor.dat'


# Read file monitor data
def load_data():
    # Load Data for first doublet (3 injection cells)
    inj1_1 = open(path1, 'r').readlines()[2::7]
    inj1_2 = open(path1, 'r').readlines()[3::7]
    inj1_3 = open(path1, 'r').readlines()[4::7]
    prod1_1 = open(path1, 'r').readlines()[5::7]
    prod1_2 = open(path1, 'r').readlines()[6::7]
    prod1_3 = open(path1, 'r').readlines()[7::7]
    # strip Data and convert to float
    inj1_1 = [i.strip().split() for i in inj1_1[:]]
    inj1_2 = [i.strip().split() for i in inj1_2[:]]
    inj1_3 = [i.strip().split() for i in inj1_3[:]]
    prod1_1 = [i.strip().split() for i in prod1_1[:]]
    prod1_2 = [i.strip().split() for i in prod1_2[:]]
    prod1_3 = [i.strip().split() for i in prod1_3[:]]
    inj1_1 = np.array(inj1_1, dtype=np.float)
    inj1_2 = np.array(inj1_2, dtype=np.float)
    inj1_3 = np.array(inj1_3, dtype=np.float)
    prod1_1 = np.array(prod1_1, dtype=np.float)
    prod1_2 = np.array(prod1_2, dtype=np.float)
    prod1_3 = np.array(prod1_3, dtype=np.float)
    # Load data into DataFrame
    inj1_1 = DataFrame(
        inj1_1,
        columns=[
            'time', 'x', 'y', 'z', 'uindex', 'i', 'j', 'k', 'head', 'temp',
            'pres', 'vx', 'vy', 'vz', 'bhpr', 'kz'
        ])
    inj1_2 = DataFrame(
        inj1_2,
        columns=[
            'time', 'x', 'y', 'z', 'uindex', 'i', 'j', 'k', 'head', 'temp',
            'pres', 'vx', 'vy', 'vz', 'bhpr', 'kz'
        ])
    inj1_3 = DataFrame(
        inj1_3,
        columns=[
            'time', 'x', 'y', 'z', 'uindex', 'i', 'j', 'k', 'head', 'temp',
            'pres', 'vx', 'vy', 'vz', 'bhpr', 'kz'
        ])
    prod1_1 = DataFrame(
        prod1_1,
        columns=[
            'time', 'x', 'y', 'z', 'uindex', 'i', 'j', 'k', 'head', 'temp',
            'pres', 'vx', 'vy', 'vz', 'bhpr', 'kz'
        ])
    prod1_2 = DataFrame(
        prod1_2,
        columns=[
            'time', 'x', 'y', 'z', 'uindex', 'i', 'j', 'k', 'head', 'temp',
            'pres', 'vx', 'vy', 'vz', 'bhpr', 'kz'
        ])
    prod1_3 = DataFrame(
        prod1_3,
        columns=[
            'time', 'x', 'y', 'z', 'uindex', 'i', 'j', 'k', 'head', 'temp',
            'pres', 'vx', 'vy', 'vz', 'bhpr', 'kz'
        ])

    #
    timeyears = np.array(prod1_1['time'] / 1)

    # calculate and plot mean temperature at producer from 3 cells
    plt.plot(
        timeyears, (prod1_1['temp'] + prod1_2['temp'] + prod1_3['temp']) / 3,
        'r',
        label=r'$50  Ls^{-1} $')

    # calculate and plot mean temperature at injector from 3 cells
    #plt.plot(timeyears,(inj1_1['temp']+inj1_2['temp']+inj1_3['temp'])/3,'r-', label=r'$50  Ls^{-1}, old SHEMAT code$')

    # only plot temp at producer, as temp at inj is constant to 30 °C
    plt.title('Temperature at Producer')
    plt.xlabel('Time [days]')
    plt.ylabel('Temperature [°C]')
    plt.legend()
    plt.ylim(40, 50)
    #plt.xlim(0,5)
    plt.savefig(
        os.environ["HOME"] +
        '/SHEMAT-Suite_Models/fw_bas_doublet_model/doc/Doublet_Temperature_at_producer.png'
    )
    plt.show()

    # Plot mean pressure at producer
    plt.plot(
        timeyears,
        (((np.asarray(prod1_1['pres'] + prod1_2['pres'] + prod1_3['pres']) /
           3))),
        'r-',
        label=r'Production $50  Ls^{-1} $')
    plt.plot(
        timeyears,
        (((np.asarray(inj1_1['pres'] + inj1_2['pres'] + inj1_3['pres']) / 3))),
        'r--',
        label=r'Injection $50  Ls^{-1} $')

    plt.xlabel('Time [days]')
    plt.ylabel('Pressure production and injection wells [MPa]')
    plt.legend()
    #plt.ylim(8,10)
    #plt.xlim(0,5)
    plt.savefig(
        os.environ["HOME"] +
        '/SHEMAT-Suite_Models/fw_bas_doublet_model/doc/Doublet_Pressure_vs_time_prod_inj.png'
    )
    plt.show()


load_data()
