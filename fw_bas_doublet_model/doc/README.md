# Model fw_bas_doublet_model #

This is a 3D geological model with a doublet system containing 1
injection and 1 production well with a production and injection rate
of 50 L/s respectively over a perforated well section of 60m.  For
running in transient model (in this case over a time period of 5
years), a steady state undisturbed input model (without doublet) has
to be generated first. The result of the steady state are loaded as initial condition to
perform the transient simulation including the doublet.  Note: The
injection and production rates of the wells are splitted over 3 cells
per well, assuming a longer perforated section.  (In some cases model
stability is enhanced by the split-up)

Results at the well positions are saved as monitor-points and can be
evaluated with the python scripts, where only the
input path to the .dat-file has to be changed to your user-specific
path.

I introduced Doublette_testmodel_short, a transient model that
calculates only the first 20 timesteps of the transient model (0.5
days) to get a test-model that is fast enough so that it can be used
for the automated testing.

Comparison of the `*_monitor.dat` files show that there are some
smaller differences in the head of the new run and the old run.

# Compilation Information
```
version_fw_gfortran_bas_none_head_master-all_quick.inc
module_fw_gfortran_bas_none_head_master-all_quick.inc
CMakeCache_fw_gfortran_bas_none_head_master-all_quick.txt
```

# Execution
```
shem_fw_gfortran_bas_none_head_master-all_quick.x > out.txt
```

date: October 2019
autor: Dominique Knapp
