# Domenico & Palciauskas model forced convection #

2D steady state model with an upper head boundary condition forcing
convection.

## Input files ##

- `DP_head`: Simulation of the head field only. The head field is
  decoupled from the temperature field in the DP-model, so this result
  should be equal to the head computed in `DP`.
- `DP_conduction`: Simulate just the linear conductive temperature
  trend according to Dirichlet temperature at top and geothermal
  gradient below.
- `DP`: Full DP-model for forced convection
- `DP_fine`: Finer version of DP-model

## Scripts ##

- `DP_head.ipy`: Plot and compare `DP_head` results.
- `DP_conduction.ipy`: Plot and compare `DP_conduction` results.
- `DP_read_SHEMAT.ipy`: Read in and plot `DP` SHEMAT-Suite results.
- `DP_analytic.ipy`: Compute and plot approximate solution from
  Domenico & Palciauskas for `DP`
- `DP_diff.ipy`: Compare analytic and computed solutions for `DP`

- `DP_head_boundary.ipy`: Compute upper head boundary for `DP`, output
  in `DP_head_boundary.txt`

## Relative errors ##

The relative error in `head` becomes smaller, when the grid is
refined, this can be seen by running `DP_fine`.

The relative error in `temp` depends on the non-dimensional constant
`K*B/alpha` (hydraulic conductivity, spread of head boundary
condition, thermal diffusivity). The smaller the constant, the smaller
the errors (but, the smaller also the deviation from the conductive
temperature field).

## Code Compilation on RWTH Cluster ##

```
Makefile_fw64int_const_none_head_master_quick.flags
module_fw64int_const_none_head_master_quick.inc
version_fw64int_const_none_head_master_quick.inc
```

Model Execution:

``` 
./shem_fw64int_const_none_head_master_quick.x > out.txt
```

## Source ##

Domenico, P. A., & Palciauskas, V. V., Theoretical analysis of forced
convective heat transfer in regional ground-water flow, Geological
Society of America Bulletin, 84(12), 3803 (1973).
http://dx.doi.org/10.1130/0016-7606(1973)84<3803:taofch>2.0.co;2

