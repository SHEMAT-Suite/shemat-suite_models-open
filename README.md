# SHEMAT-Suite_Models-open #

This repository contains input files for
[SHEMAT-Suite](https://git.rwth-aachen.de/SHEMAT-Suite/SHEMAT-Suite-open)
input files and the corresponding results. These results serve as a
benchmark tests for new developments of `SHEMAT-Suite`.

It is suggested to clone the repository using

```
git clone git@git.rwth-aachen.de:SHEMAT-Suite/shemat-suite_models-open.git SHEMAT-Suite_Models
```

If you clone it from your home directory, the repository will be
located at `~/SHEMAT-Suite_Models`. This is the default configuration
implemented in the compilation scripts `compilequick.sh`.

## Benchmarks for SHEMAT-Suite

This is a list of the benchmark models for SHEMAT-Suite for which the
input files are available in this repository. For more information on
the models see the tables below.

| **Benchmarks for SHEMAT-Suite**        | `Source`                                                                                                                                                                                                                                                        |
|:--------------------------------------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| `fw_basc_ElderProblem`                 | Elder, J., Numerical experiments with free convection in a vertical slot, Journal of Fluid Mechanics, 24(4), 823–843 (1966).                                                                                                                                    |
| `fw_basc_HenryProblem`                 | Thomas, A. T., Reiche, S., Riedel, M., & Clauser, C., The fate of submarine fresh groundwater reservoirs at the new jersey shelf, usa, Hydrogeology Journal, 27(7), 2673–2694 (2019).  http://dx.doi.org/10.1007/s10040-019-01997-y                             |
| `fw_const_DP`                          | Domenico, P. A., & Palciauskas, V. V., Theoretical analysis of forced convective heat transfer in regional ground-water flow, Geological Society of America Bulletin, 84(12), 3803 (1973). http://dx.doi.org/10.1130/0016-7606(1973)84<3803:taofch>2.0.co;2     |
| `fw_const_HeatConduction1D`            | simple analytical solution                                                                                                                                                                                                                                      |
| `fw_const_Peclet`                      | simple analytical solution                                                                                                                                                                                                                                      |
| `fw_const_Theis_analytical_comparison` | Theis, C. V., The relation between the lowering of the piezometric surface and the rate and duration of discharge of a well using ground-water storage, Transactions, American Geophysical Union, 16(2), 519 (1935).  http://dx.doi.org/10.1029/tr016i002p00519 |
| `fw_const_Thiem`                       | Batu, V., Aquifer hydraulics : a comprehensive guide to hydrogeologic data analysis (1998), : Wiley, New York.                                                                                                                                                  |
| `fw_pres_const_DP`                     | Domenico, P. A., & Palciauskas, V. V., Theoretical analysis of forced convective heat transfer in regional ground-water flow, Geological Society of America Bulletin, 84(12), 3803 (1973). http://dx.doi.org/10.1130/0016-7606(1973)84<3803:taofch>2.0.co;2     |
| `fw_const_IES`                     | Kauerauf, Armin, IES Benchmark Test June 2002, IES Integrated Exploaration Systems, Jülich, Germany (2002)    |

A code demonstration with the Theis model (`fw_const_Theis_analytical_comparison`) is available as Code Ocean Capsule: https://codeocean.com/capsule/4639982/tree.

## Conventions

The testmodels should use **Exactly one executable per model
directory**!

Thus, **multiple Input Files in one model directory should all run
with the same executable**!

### Directory naming convention:

The following naming convention is used for test directories

`runmode_propsmodule_description`


Examples:

`fw_bas_2D-Heat-Transfer`\
`fw_mphase_const_2D-CO2-injection`\
`sm_bas_MonteCarlo`


Every Model folder should contain:

- a `/doc` folder that describes how to compile and run the model
- a `/result` folder for collecting the output

## Current number of testmodels in open benchmark ##

```
==============>>>>>>>>>>>>>>    27    <<<<<<<<<<<<<<==============
```

## doc ##

### Model description ###

A short description of the model should be put in the `/doc`
folder. This should first include a short explanation of the physics
behind the model. It can also contain a small note about the runtime
needed and the number of processes used to obtain this runtime.

### Compilation documentation ##

Please follow the [Best
Practices](https://git.rwth-aachen.de/SHEMAT-Suite/SHEMAT-Suite-open/-/wikis/tutorial/compilation#compilation-best-practices)
from the SHEMAT-Suite-Wiki and put the corresponding output files in
the `/doc` path.


## deltatests ##

In the directory `deltatests`, there are Python-routines for testing
new `SHEMAT-Suite` output against the output stored in the `/result`
folder of the model directories (example result folder:
`SHEMAT-Suite_Models/fw_const_Theis_analytical_comparison/result`).

### Prerequisites ###

- SHEMAT-Suite `v8.00` or newer
- Python, version `3.7` or newer

### Usage of deltatests ###

To use the routines, change directory to
`SHEMAT-Suite_Models/deltatests`.  Check and possibly change the input
at the beginning of the script `runSHEMATtest.ipy` below

``` python
###############################################################################
#                                    Input                                    #
###############################################################################
```

Go to `SHEMAT-Suite_Models/deltatests/site-packages/`, open
`SHEMATtest_dirs.py`, and include your local path configuration.

``` python
# Path configuration

shemat_suite_models_dir = os.environ['HOME']+'/'+'SHEMAT-Suite_Models'

make_dir = os.environ['HOME'] + "/SHEMAT-Suite"
```

The current configuration works, if the git repositories
`SHEMAT-Suite` and `SHEMAT-Suite_Models` are located in the home
directory.

When the setup is complete, execute the IPython script, for example by
entering the following command, or in your own Python-setup.

``` shell
python runSHEMATtest.ipy
```

### Adding new deltatests ###

To add new tests, you have to add information about the new test
directory at a number of places.
1. Add the new test model in `runSHEMATtest.ipy`. Add the name of the
directory.
2. Add an input file analogous to
`deltatests/site-packages/input/input_fw_const_Theis_analytical_comparison.py`. This
should be possible by copying this file, changing the directory
specifications. Please look at each input and change the input
according to your new model. Most of the inputs should be
self-explanatory.
3. Add an `import` statement for the new input file in
   `SHEMATtest_input.py`. Finally add the new directory in the
   function `set_input` in the same file.

**Warning**: When adding new models, do not use the option
`is_cleanup` in `runSHEMATtest.ipy`, before committing changes to the
git repository (Reason: `is_cleanup = True` will remove all
uncommitted changes in a model directory!)


## Matrix of models against features ##

| **Models**                               | **Small description**                                                   |
|:----------------------------------------:|:-----------------------------------------------------------------------:|
| `ad_const_TemperatureInversion`          | Testmodel for deterministic inversion (AD mode)                         |
| `ad_pres_const_TemperatureInversion`     | Testmodel for deterministic inversion (AD mode, pressure-based)         |
| `fw_bas_2Dfancy`                         | 2D benchmark model for forced and free convection                       |
| `fw_bas_Head_Pres`                       | Static head-temperature model, bas-density                              |
| `fw_bas_TinyAdvective`                   | Testmodel for Advective Heat Transport                                  |
| `fw_bas_simpleConvection`                | 3D Steady-state and transient simple temperature boundary condition     |
| `fw_bas_doublet_model`                   | 3D simulation of the transient operation of a geothermal doublet        |
| `fw_basc_ElderProblem`                   | Steady-state and transient Elder Problem                                |
| `fw_basc_HenryProblem`                   | 2D Henry Problem: concentration entering flow regime                    |
| `fw_basc_NeumannBox`                     | small 3D internal Neumann boundary condition                            |
| `fw_basc_SaltFlow`                       | Model Info: Simple 2D transient model for flow and salt transport.      |
| `fw_const_DP`                            | Forced Convective Heat Transfer                                         |
| `fw_const_Head_Pres`                     | Static head-temperature model, constant density                         |
| `fw_const_HeatConduction1D`              | Transient 1D heat conduction                                            |
| `fw_const_Peclet`                        | vertical temperature profile influenced by vertical flow                |
| `fw_const_Theis_analytical_comparison`   | transient Theis problem with analytical comparison                      |
| `fw_const_Thiem`                         | steady-state Thiem problem with analytical comparison                   |
| `fw_const_IES`                           | Three 2D conduction benchmark models                                    |
| `fw_ice_GeoSlope`                        | 2D ice module testmodel                                                 |
| `fw_ice_stefanproblem`                   | 1D Stefan Problem: Melting ice from one side                            |
| `fw_pres_bas_2Dfancy`                    | 2D benchmark model for forced and free convection, pressure formulation |
| `fw_pres_bas_Head_Pres`                  | Static pres-temperature model, bas-density                              |
| `fw_pres_const_DP`                       | Forced Convective Heat Transfer, pressure formulation                   |
| `fw_pres_const_Head_Pres`                | Static pres-temperature model, constant density                         |
| `sm_const_MC_small`                      | steady state flow for four random permeability realizations             |
| `sm_const_wavereal_enkf`                 | Single EnKF-update based on 2D concentration                            |
| `sm_const_wavereal_true`                 | 2D concentration flow through SGSim-generated permeability field        |

| **Runmodes**                             | `fw` | `sm` | `ad` |
|:----------------------------------------:|:----:|:----:|:----:|
| `ad_const_TemperatureInversion`          |      |      | x    |
| `ad_pres_const_TemperatureInversion`     |      |      | x    |
| `fw_bas_2Dfancy`                         | x    |      |      |
| `fw_bas_Head_Pres`                       | x    |      |      |
| `fw_bas_TinyAdvective`                   | x    |      |      |
| `fw_bas_simpleConvection`                | x    |      |      |
| `fw_bas_doublet_model`                   | x    |      |      |
| `fw_basc_ElderProblem`                   | x    |      |      |
| `fw_basc_HenryProblem`                   | x    |      |      |
| `fw_basc_NeumannBox`                     | x    |      |      |
| `fw_basc_SaltFlow`                       | x    |      |      |
| `fw_const_DP`                            | x    |      |      |
| `fw_const_Head_Pres`                     | x    |      |      |
| `fw_const_HeatConduction1D`              | x    |      |      |
| `fw_const_Peclet`                        | x    |      |      |
| `fw_const_Theis_analytical_comparison`   | x    |      |      |
| `fw_const_Thiem`                         | x    |      |      |
| `fw_const_IES`                           | x    |      |      |
| `fw_ice_GeoSlope`                        | x    |      |      |
| `fw_ice_stefanproblem`                   | x    |      |      |
| `fw_pres_bas_2Dfancy`                    | x    |      |      |
| `fw_pres_bas_Head_Pres`                  | x    |      |      |
| `fw_pres_const_DP`                       | x    |      |      |
| `fw_pres_const_Head_Pres`                | x    |      |      |
| `sm_const_MC_small`                      |      | x    |      |
| `sm_const_wavereal_enkf`                 |      | x    |      |
| `sm_const_wavereal_true`                 |      | x    |      |

| **Modes**                                | `head` | `pres` |
|:----------------------------------------:|:------:|:------:|
| `ad_const_TemperatureInversion`          | x      |        |
| `ad_pres_const_TemperatureInversion`     |        | x      |
| `fw_bas_2Dfancy`                         | x      |        |
| `fw_bas_Head_Pres`                       | x      |        |
| `fw_bas_TinyAdvective`                   | x      |        |
| `fw_bas_simpleConvection`                | x      |        |
| `fw_bas_doublet_model`                   | x      |        |
| `fw_basc_ElderProblem`                   | x      |        |
| `fw_basc_HenryProblem`                   | x      |        |
| `fw_basc_NeumannBox`                     | x      |        |
| `fw_basc_SaltFlow`                       | x      |        |
| `fw_const_DP`                            | x      |        |
| `fw_const_Head_Pres`                     | x      |        |
| `fw_const_HeatConduction1D`              | x      |        |
| `fw_const_Peclet`                        | x      |        |
| `fw_const_Theis_analytical_comparison`   | x      |        |
| `fw_const_Thiem`                         | x      |        |
| `fw_const_IES`                           | x      |        |
| `fw_ice_GeoSlope`                        | x      |        |
| `fw_ice_stefanproblem`                   | x      |        |
| `fw_pres_bas_2Dfancy`                    |        | x      |
| `fw_pres_bas_Head_Pres`                  |        | x      |
| `fw_pres_const_DP`                       |        | x      |
| `fw_pres_const_Head_Pres`                |        | x      |
| `sm_const_MC_small`                      | x      |        |
| `sm_const_wavereal_enkf`                 | x      |        |
| `sm_const_wavereal_true`                 | x      |        |


| **Props**                                | `const` | `bas` | `basc` | `gheexpl` | `ice` |
|:----------------------------------------:|:-------:|:-----:|:------:|:---------:|:------|
| `ad_const_TemperatureInversion`          | x       |       |        |           |       |
| `ad_pres_const_TemperatureInversion`     | x       |       |        |           |       |
| `fw_bas_2Dfancy`                         |         | x     |        |           |       |
| `fw_bas_Head_Pres`                       |         | x     |        |           |       |
| `fw_bas_TinyAdvective`                   |         | x     |        |           |       |
| `fw_bas_simpleConvection`                |         | x     |        |           |       |
| `fw_bas_doublet_model`                   |         | x     |        |           |       |
| `fw_basc_ElderProblem`                   |         |       | x      |           |       |
| `fw_basc_HenryProblem`                   |         |       | x      |           |       |
| `fw_basc_NeumannBox`                     |         |       | x      |           |       |
| `fw_basc_SaltFlow`                       |         |       | x      |           |       |
| `fw_const_DP`                            | x       |       |        |           |       |
| `fw_const_Head_Pres`                     | x       |       |        |           |       |
| `fw_const_HeatConduction1D`              | x       |       |        |           |       |
| `fw_const_Peclet`                        | x       |       |        |           |       |
| `fw_const_Theis_analytical_comparison`   | x       |       |        |           |       |
| `fw_const_Thiem`                         | x       |       |        |           |       |
| `fw_const_IES`                           | x       |       |        |           |       |
| `fw_ice_GeoSlope`                        |         |       |        |           | x     |
| `fw_ice_stefanproblem`                   |         |       |        |           | x     |
| `fw_pres_bas_2Dfancy`                    |         | x     |        |           |       |
| `fw_pres_bas_Head_Pres`                  |         | x     |        |           |       |
| `fw_pres_const_DP`                       | x       |       |        |           |       |
| `fw_pres_const_Head_Pres`                | x       |       |        |           |       |
| `sm_const_MC_small`                      | x       |       |        |           |       |
| `sm_const_wavereal_enkf`                 | x       |       |        |           |       |
| `sm_const_wavereal_true`                 | x       |       |        |           |       |

| **Variables**                            | `head`/`pres` | `temp` | `conc` | `head/temp` / `pres/temp` | `head/conc` / `pres/conc` | `head/temp/conc` / `pres/temp/conc` |
|:----------------------------------------:|:-------------:|:------:|:------:|:-------------------------:|:-------------------------:|:-----------------------------------:|
| `ad_const_TemperatureInversion`          |               |        |        | x                         |                           |                                     |
| `ad_pres_const_TemperatureInversion`     |               |        |        | x                         |                           |                                     |
| `fw_bas_2Dfancy`                         |               |        |        | x                         |                           |                                     |
| `fw_bas_Head_Pres`                       | x             | x      |        | x                         |                           |                                     |
| `fw_bas_TinyAdvective`                   |               |        |        | x                         |                           |                                     |
| `fw_bas_simpleConvection`                |               |        |        | x                         |                           |                                     |
| `fw_bas_doublet_model`                   |               |        |        | x                         |                           |                                     |
| `fw_basc_ElderProblem`                   |               |        |        |                           | x                         |                                     |
| `fw_basc_HenryProblem`                   |               |        |        |                           | x                         |                                     |
| `fw_basc_NeumannBox`                     | x             |        |        |                           |                           |                                     |
| `fw_basc_SaltFlow`                       |               |        |        |                           | x                         |                                     |
| `fw_const_DP`                            | x             | x      |        | x                         |                           |                                     |
| `fw_const_Head_Pres`                     | x             | x      |        | x                         |                           |                                     |
| `fw_const_HeatConduction1D`              |               | x      |        |                           |                           |                                     |
| `fw_const_Peclet`                        |               |        |        | x                         |                           |                                     |
| `fw_const_Theis_analytical_comparison`   | x             |        |        |                           |                           |                                     |
| `fw_const_Thiem`                         | x             |        |        |                           |                           |                                     |
| `fw_const_IES`                           |               | x      |        |                           |                           |                                     |
| `fw_ice_GeoSlope`                        |               |        |        | x                         |                           |                                     |
| `fw_ice_stefanproblem`                   |               | x      |        |                           |                           |                                     |
| `fw_pres_bas_2Dfancy`                    |               |        |        | x                         |                           |                                     |
| `fw_pres_bas_Head_Pres`                  | x             | x      |        | x                         |                           |                                     |
| `fw_pres_const_DP`                       |               |        |        | x                         |                           |                                     |
| `fw_pres_const_Head_Pres`                | x             | x      |        | x                         |                           |                                     |
| `sm_const_MC_small`                      | x             |        |        |                           |                           |                                     |
| `sm_const_wavereal_enkf`                 |               |        |        |                           | x                         |                                     |
| `sm_const_wavereal_true`                 |               |        |        |                           | x                         |                                     |

| **Grid**                                 | `1D` | `2D` | `3D` |
|:----------------------------------------:|:----:|------|:----:|
| `ad_const_TemperatureInversion`          |      | x    |      |
| `ad_pres_const_TemperatureInversion`     |      | x    |      |
| `fw_bas_2Dfancy`                         |      | x    |      |
| `fw_bas_Head_Pres`                       |      | x    |      |
| `fw_bas_TinyAdvective`                   |      | x    |      |
| `fw_bas_simpleConvection`                |      |      | x    |
| `fw_bas_doublet_model`                   |      |      | x    |
| `fw_basc_ElderProblem`                   |      | x    |      |
| `fw_basc_HenryProblem`                   |      | x    |      |
| `fw_basc_NeumannBox`                     |      |      | x    |
| `fw_basc_SaltFlow`                       |      | x    |      |
| `fw_const_DP`                            |      | x    |      |
| `fw_const_Head_Pres`                     |      | x    |      |
| `fw_const_HeatConduction1D`              | x    |      |      |
| `fw_const_Peclet`                        | (x)  | x    |      |
| `fw_const_Theis_analytical_comparison`   |      | x    |      |
| `fw_const_Thiem`                         |      | x    |      |
| `fw_const_IES`                           |      | x    |      |
| `fw_ice_GeoSlope`                        |      | x    |      |
| `fw_ice_stefanproblem`                   | (x)  | x    |      |
| `fw_pres_bas_2Dfancy`                    |      | x    |      |
| `fw_pres_bas_Head_Pres`                  |      | x    |      |
| `fw_pres_const_DP`                       |      | x    |      |
| `fw_pres_const_Head_Pres`                |      | x    |      |
| `sm_const_MC_small`                      |      | x    |      |
| `sm_const_wavereal_enkf`                 |      | x    |      |
| `sm_const_wavereal_true`                 |      | x    |      |

**(x)**: quasi-dimension, only effects in this number of dimensions


| **Time**                                 | `Steady State` | `Transient` |
|:----------------------------------------:|:--------------:|-------------|
| `ad_const_TemperatureInversion`          | x              |             |
| `ad_pres_const_TemperatureInversion`     | x              |             |
| `fw_bas_2Dfancy`                         | x              |             |
| `fw_bas_Head_Pres`                       | x              |             |
| `fw_bas_TinyAdvective`                   | x              |             |
| `fw_bas_simpleConvection`                | x              | x           |
| `fw_bas_doublet_model`                   | x              | x           |
| `fw_basc_ElderProblem`                   | x              | x           |
| `fw_basc_HenryProblem`                   | x              |             |
| `fw_basc_NeumannBox`                     | x              |             |
| `fw_basc_SaltFlow`                       |                | x           |
| `fw_const_DP`                            | x              |             |
| `fw_const_Head_Pres`                     | x              |             |
| `fw_const_HeatConduction1D`              |                | x           |
| `fw_const_Peclet`                        | x              |             |
| `fw_const_Theis_analytical_comparison`   |                | x           |
| `fw_const_Thiem`                         | x              |             |
| `fw_const_IES`                           | x              | x           |
| `fw_ice_GeoSlope`                        | x              |             |
| `fw_ice_stefanproblem`                   |                | x           |
| `fw_pres_bas_2Dfancy`                    | x              |             |
| `fw_pres_bas_Head_Pres`                  | x              |             |
| `fw_pres_const_DP`                       | x              |             |
| `fw_pres_const_Head_Pres`                | x              |             |
| `sm_const_MC_small`                      | x              |             |
| `sm_const_wavereal_enkf`                 |                | x           |
| `sm_const_wavereal_true`                 |                | x           |

| **Inversion**                            | `Deterministic Inversion` | `EnKF` |
|:----------------------------------------:|:-------------------------:|:------:|
| `ad_const_TemperatureInversion`          | x                         |        |
| `ad_pres_const_TemperatureInversion`     | x                         |        |
| `fw_bas_2Dfancy`                         |                           |        |
| `fw_bas_Head_Pres`                       |                           |        |
| `fw_bas_TinyAdvective`                   |                           |        |
| `fw_bas_simpleConvection`                |                           |        |
| `fw_bas_doublet_model`                   |                           |        |
| `fw_basc_ElderProblem`                   |                           |        |
| `fw_basc_HenryProblem`                   |                           |        |
| `fw_basc_NeumannBox`                     |                           |        |
| `fw_basc_SaltFlow`                       |                           |        |
| `fw_const_DP`                            |                           |        |
| `fw_const_Head_Pres`                     |                           |        |
| `fw_const_HeatConduction1D`              |                           |        |
| `fw_const_Peclet`                        |                           |        |
| `fw_const_Theis_analytical_comparison`   |                           |        |
| `fw_const_Thiem`                         |                           |        |
| `fw_const_IES`                           |                           |        |
| `fw_ice_GeoSlope`                        |                           |        |
| `fw_ice_stefanproblem`                   |                           |        |
| `fw_pres_bas_2Dfancy`                    |                           |        |
| `fw_pres_bas_Head_Pres`                  |                           |        |
| `fw_pres_const_DP`                       |                           |        |
| `fw_pres_const_Head_Pres`                |                           |        |
| `sm_const_MC_small`                      |                           |        |
| `sm_const_wavereal_enkf`                 |                           | x      |
| `sm_const_wavereal_true`                 |                           |        |

| **Cluster architecture**                 | `serial` | `openmp` | `mpi` | `Slurm Job` |
|:----------------------------------------:|:--------:|:--------:|:-----:|:-----------:|
| `ad_const_TemperatureInversion`          | x        |          |       |             |
| `ad_pres_const_TemperatureInversion`     | x        |          |       |             |
| `fw_bas_2Dfancy`                         | x        |          |       |             |
| `fw_bas_Head_Pres`                       | x        |          |       |             |
| `fw_bas_TinyAdvective`                   | x        |          |       |             |
| `fw_bas_simpleConvection`                | x        |          |       |             |
| `fw_bas_doublet_model`                   | x        |          |       |             |
| `fw_basc_ElderProblem`                   | x        |          |       |             |
| `fw_basc_HenryProblem`                   | x        |          |       |             |
| `fw_basc_NeumannBox`                     | x        |          |       |             |
| `fw_basc_SaltFlow`                       | x        |          |       |             |
| `fw_const_DP`                            | x        |          |       |             |
| `fw_const_Head_Pres`                     | x        |          |       |             |
| `fw_const_HeatConduction1D`              | x        |          |       |             |
| `fw_const_Peclet`                        | x        |          |       |             |
| `fw_const_Theis_analytical_comparison`   | x        |          |       |             |
| `fw_const_Thiem`                         | x        |          |       |             |
| `fw_const_IES`                           | x        |          |       |             |
| `fw_ice_GeoSlope`                        | x        |          |       |             |
| `fw_ice_stefanproblem`                   | x        |          |       |             |
| `fw_pres_bas_2Dfancy`                    | x        |          |       |             |
| `fw_pres_bas_Head_Pres`                  | x        |          |       |             |
| `fw_pres_const_DP`                       | x        |          |       |             |
| `fw_pres_const_Head_Pres`                | x        |          |       |             |
| `sm_const_wavereal_enkf`                 | x        |          |       |             |
| `sm_const_wavereal_true`                 | x        |          |       |             |

| **Verification method**                  | `previous simulation` | `analytical solution`             | `semi-analytical solution` |
|:----------------------------------------:|:---------------------:|:---------------------------------:|:--------------------------:|
| `ad_const_TemperatureInversion`          | x                     |                                   |                            |
| `ad_pres_const_TemperatureInversion`     | x                     |                                   |                            |
| `fw_bas_2Dfancy`                         | x                     |                                   |                            |
| `fw_bas_Head_Pres`                       | x                     | planned                           |                            |
| `fw_bas_TinyAdvective`                   | x                     |                                   |                            |
| `fw_bas_simpleConvection`                | x                     |                                   |                            |
| `fw_bas_doublet_model`                   | x                     |                                   |                            |
| `fw_basc_ElderProblem`                   | x                     | x, larger model in bachelorthesis |                            |
| `fw_basc_HenryProblem`                   | x                     |                                   | x, visual                  |
| `fw_basc_NeumannBox`                     | x                     |                                   |                            |
| `fw_basc_SaltFlow`                       | x                     |                                   |                            |
| `fw_const_DP`                            | x                     | x, Python scripts                 |                            |
| `fw_const_Head_Pres`                     | x                     | planned                           |                            |
| `fw_const_HeatConduction1D`              | x                     | x, Python/Matlab scripts          |                            |
| `fw_const_Peclet`                        | x                     | x, Python script                  |                            |
| `fw_const_Theis_analytical_comparison`   | x                     | x, Python script                  |                            |
| `fw_const_Thiem`                         | x                     | x, Python script                  |                            |
| `fw_const_IES`                           | x                     | x, Python script                  |                            |
| `fw_ice_GeoSlope`                        | x                     |                                   |                            |
| `fw_ice_stefanproblem`                   | x                     | x, Python script/visual           |                            |
| `fw_pres_bas_2Dfancy`                    | x                     |                                   |                            |
| `fw_pres_bas_Head_Pres`                  | x                     | planned                           |                            |
| `fw_pres_const_DP`                       | x                     | x, Python scripts                 |                            |
| `fw_pres_const_Head_Pres`                | x                     | planned                           |                            |
| `sm_const_MC_small`                      | x                     |                                   |                            |
| `sm_const_wavereal_enkf`                 | x                     |                                   |                            |
| `sm_const_wavereal_true`                 | x                     |                                   |                            |
