# Testmodel for deterministic inversion (AD mode) #

* 2D flow and heat transport model
* geology simplified from Perth basin
* layered structure with permeable fault 
* Temperature data are inverted to determine permeability of unit 5
* model by Jan Niederau
* contact person: Johanna Bruckmann

See file `Documentation_od_Perth_inputs.doc` for more information.

## Python scripts ##

- `pressure_init.ipy`: Pressure field for forward temperature
  computation in `oed_perth_init`
- `pressure_bcd_left.ipy`: Pressure boundary condition for inversion
  in `oed_inv`

## Compilation ##

The compilation information for the forward run
```
CMakeCache_fw_ifort_const_none_pres_cmake_quick.txt
module_fw_ifort_const_none_pres_cmake_quick.inc
version_fw_ifort_const_none_pres_cmake_quick.inc
```

The compilation information for the inversion with `ad`:
```
CMakeCache_ad_ifort_const_none_pres_cmake_quick.txt
module_ad_ifort_const_none_pres_cmake_quick.inc
version_ad_ifort_const_none_pres_cmake_quick.inc
```

## Execution ##

``` shell
./shem_fw_ifort_const_none_pres_cmake_quick.x
./shem_ad_ifort_const_none_pres_cmake_quick.x
```
