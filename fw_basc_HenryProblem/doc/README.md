# Model info: #
In the Henry saltwater intrusion problem, saltwater is coming into the
system from one side (right) and fresh water from the other side
(left).  For a more detailed description read
`Modified-Henry-Problem_documentation.pdf`.

In the comparison repository the files `henry_head.txt`,
`henry_salinity.txt`, `henry_velocity_field.txt` and `henry_XYZ.txt`
are used in the plot_velocity.m. These are the results of the
computation, out of the
henry_final.vtk. `analytic_sol_simpson-and-clement-2004.txt` is also
used in the matlab script.

# Compilation on RWTH Cluster #

```
gmake fw hdf noomp PROPS=basc HDF5_MOD=/usr/local_rwth/sw/HDF5/1.10.4/intel_19.0.1.144-intelmpi_2018.4.274/include/ HDF5_LIB=/usr/local_rwth/sw/HDF5/1.10.4/intel_19.0.1.144-intelmpi_2018.4.274/lib/ COMPTYPE=lini64
```

Compilation outputs:

```
Makefile_fw64int_basc_master_individual.flags
module_fw64int_basc_master_individual.inc
version_fw64int_basc_master_individual.inc
```

Execution:

```
./shem_fw64int_basc_head_master.x
```

# Background information #

In the course of two bachelor theses the coupling between tracer
transport and groundwater flow have been tested using the Elder
problem and the Henry problem.

The bachelortheses can be found here:
`W:\GGE_Public\SHEMAT`

Henry problem
=============

The Henry problem was the topic of a Bachelorthesis (Modeling of salt
transport processes in passive continental margin sediments) written
by Fabian Patterson at the GGE in August 2014 with the following
Abstract (in German, Corrrections in the printed manuscript included):

`Die Vorliegende Bachelorarbeit beinhaltet die Untersuchung des Henry-Problems mit SHEMAT-Suite, sowie eine Anwendung des`\
`Henry-Problems auf die Salztransportprozesse in einem einfachen Kontinentalrandmodell. Ziel der Arbeit ist es die `\
`Funktionsfähigkeit von SHEMAT-Suite für die Modellierung von hydrodynamischen Salztransportprozessen zu überprüfen. Darüber`\
`hinaus soll das einfache Kontinentalrandmodell als eine Voruntersuchung für weitere, umfangreichere Modellierungen von `\
`Transportprozessen in Schelfsedimenten dienen.`\ \
`Die Modellierung des Henry-Problems wird als Benchmark für dichtegetriebene Transportprozesse genutzt. Die numerische `\
`Lösung des Henry-Problems mit SHEMAT-Suite erzeugt eine hinreichend genau Reproduktion der semi-analytischen Lösung von `\
`Henry(1964). Ein weiterer Vergleich mit anderen numerischen Lösungen z. B. Croucher & O'Sullivan (1995) ergibt fast eine`\
`komplette Übereinstimmung. SHEMAT-Suite ist somit in der Lage dichtegetriebene Transportprozesse abzubilden.`\
\
`Die Erstellung des Schelfmodells orientiert sich stark an den Ergebnissen des IODP Projekts 313. Die hydraulisch wichtigen`\
`Parameter des Modells wurdena us den petrophysikalischen Untersuchungen der Bohrkerne entnommen. Die Geometrie des Modells`\
`orientiert sich sowohl an seismischen Untersuchungen als auch an den Bohrprofilen. Die im Rahmen des IODP Projekts 313 `\
`gemessenen Salzkonzentrationen konnten mit dem SHEMAT-Suite Modell nicht reproduziert werden, jedoch konnte ein Zusammenhang`\
`zwischen der Salzkonzentration innerhalb der Sedimente und deren Permeabilität nachgestellt werden.`

Sources in the Abstract:

-   Croucher, A. E., O'Sullivan, M. J., 1995, The Henry problem for
    saltwater intrusion, Water Resources Research, Vol. 31, No. 7,
    1809-1814.
-   Henry, H. R. 1964, Effects of dispersion on salt encroachment in
    coastal aquifers. Sea Water in Coastal Aquifers, U.S. Geol. Surv.
    Supply Pap., 1613-C, C71-C84.
