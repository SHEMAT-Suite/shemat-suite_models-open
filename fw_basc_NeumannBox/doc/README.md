# Internal Head Neumann Boundary condition #

3D steady state model with an internal head Neumann boundary
condition. `basc` version.

It also exists a `const` and `bas` version of this model:
`fw_const_NeumannBox`
`fw_bas_NeumannBox`

## Code Compilation on RWTH Cluster ##
```
Makefile_fw64int_basc_master_quick.flags
module_fw64int_basc_master_quick.inc
version_fw64int_basc_master_quick.inc
```

# Model Execution:
``` 
./shem_fw64int_basc_head_master.x
```
