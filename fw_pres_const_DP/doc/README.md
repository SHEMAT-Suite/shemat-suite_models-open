# Domenico & Palciauskas model forced convection, pressure version #

2D steady state model with an upper pressure boundary condition
forcing convection.

## Input files ##

- `DP`: Full DP-model for forced convection

## Scripts ##

- `DP_compare_with_head.ipy`: Compare results of the pressure
  computation with the results of the head computation in
  `fw_const_DP/`

- `DP_read_SHEMAT.ipy`: Read in and plot `DP` SHEMAT-Suite results.
- `DP_analytic.ipy`: Compute and plot approximate solution from
  Domenico & Palciauskas for `DP`
- `DP_diff.ipy`: Compare analytic and computed solutions for `DP`

- `DP_pres_boundary.ipy`: Compute upper pressure boundary for `DP`,
  output in `DP_pres_boundary.txt`

## Code Compilation on RWTH Cluster ##

```
Makefile_fw64int_const_none_pres_master_quick.flags
module_fw64int_const_none_pres_master_quick.inc
version_fw64int_const_none_pres_master_quick.inc
```

Model Execution:

``` 
./shem_fw64int_const_none_pres_master_quick.x > out.txt
```

## Source ##

Domenico, P. A., & Palciauskas, V. V., Theoretical analysis of forced
convective heat transfer in regional ground-water flow, Geological
Society of America Bulletin, 84(12), 3803 (1973).
http://dx.doi.org/10.1130/0016-7606(1973)84<3803:taofch>2.0.co;2

