# This is the CMakeCache file.
# For build in directory: /home/mv897575/SHEMAT-Suite/build_const
# It was generated by CMake: /rwthfs/rz/SW/UTIL.common/cmake/3.16.4/bin/cmake
# You can edit this file to change values found and used by cmake.
# If you do not want to change any of the values, simply exit the editor.
# If you do want to change a value, simply edit, save, and exit the editor.
# The syntax for the file is as follows:
# KEY:TYPE=VALUE
# KEY is the name of a variable in the cache.
# TYPE is a hint to GUIs for the type of VALUE, DO NOT EDIT TYPE!.
# VALUE is the current value for the KEY.

########################
# EXTERNAL cache entries
########################

//Used AD forward mode directories
ADTYPE:STRING=g_tap

//Used AD reverse mode directories
AD_RMTYPE:STRING=ad_tap

//Path to a library.
BLAS_guide_LIBRARY:FILEPATH=BLAS_guide_LIBRARY-NOTFOUND

//Path to a library.
BLAS_iomp5_LIBRARY:FILEPATH=/opt/intel/Compiler/19.0/1.144/rwthlnk/compiler/lib/intel64_lin/libiomp5.so

//Path to a library.
BLAS_mkl_core_LIBRARY:FILEPATH=/opt/intel/Compiler/19.0/1.144/rwthlnk/mkl/lib/intel64_lin/libmkl_core.so

//Path to a library.
BLAS_mkl_intel_LIBRARY:FILEPATH=/opt/intel/Compiler/19.0/1.144/rwthlnk/mkl/lib/ia32_lin/libmkl_intel.so

//Path to a library.
BLAS_mkl_intel_lp64_LIBRARY:FILEPATH=/opt/intel/Compiler/19.0/1.144/rwthlnk/mkl/lib/intel64_lin/libmkl_intel_lp64.so

//Path to a library.
BLAS_mkl_intel_thread_LIBRARY:FILEPATH=/opt/intel/Compiler/19.0/1.144/rwthlnk/mkl/lib/intel64_lin/libmkl_intel_thread.so

//Path to a program.
CMAKE_ADDR2LINE:FILEPATH=/usr/bin/addr2line

//Path to a program.
CMAKE_AR:FILEPATH=/usr/bin/ar

//Choose the type of build, options are: None Debug Release RelWithDebInfo
// MinSizeRel ...
CMAKE_BUILD_TYPE:STRING=

//Enable/Disable color output during build.
CMAKE_COLOR_MAKEFILE:BOOL=ON

//CXX compiler
CMAKE_CXX_COMPILER:FILEPATH=/opt/intel/Compiler/19.0/1.144/rwthlnk/bin/intel64/icpc

//Flags used by the CXX compiler during all build types.
CMAKE_CXX_FLAGS:STRING=

//Flags used by the CXX compiler during DEBUG builds.
CMAKE_CXX_FLAGS_DEBUG:STRING=-g

//Flags used by the CXX compiler during MINSIZEREL builds.
CMAKE_CXX_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the CXX compiler during RELEASE builds.
CMAKE_CXX_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the CXX compiler during RELWITHDEBINFO builds.
CMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

//C compiler
CMAKE_C_COMPILER:FILEPATH=/opt/intel/Compiler/19.0/1.144/rwthlnk/bin/intel64/icc

//Flags used by the C compiler during all build types.
CMAKE_C_FLAGS:STRING=

//Flags used by the C compiler during DEBUG builds.
CMAKE_C_FLAGS_DEBUG:STRING=-g

//Flags used by the C compiler during MINSIZEREL builds.
CMAKE_C_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the C compiler during RELEASE builds.
CMAKE_C_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the C compiler during RELWITHDEBINFO builds.
CMAKE_C_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

//Path to a program.
CMAKE_DLLTOOL:FILEPATH=CMAKE_DLLTOOL-NOTFOUND

//Flags used by the linker during all build types.
CMAKE_EXE_LINKER_FLAGS:STRING=

//Flags used by the linker during DEBUG builds.
CMAKE_EXE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during MINSIZEREL builds.
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during RELEASE builds.
CMAKE_EXE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during RELWITHDEBINFO builds.
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Enable/Disable output of compile commands during generation.
CMAKE_EXPORT_COMPILE_COMMANDS:BOOL=OFF

//Fortran compiler
CMAKE_Fortran_COMPILER:FILEPATH=/opt/intel/Compiler/19.0/1.144/rwthlnk/bin/intel64/ifort

//Flags used by the Fortran compiler during all build types.
CMAKE_Fortran_FLAGS:STRING=

//Flags used by the Fortran compiler during DEBUG builds.
CMAKE_Fortran_FLAGS_DEBUG:STRING=-g

//Flags used by the Fortran compiler during MINSIZEREL builds.
CMAKE_Fortran_FLAGS_MINSIZEREL:STRING=-Os

//Flags used by the Fortran compiler during RELEASE builds.
CMAKE_Fortran_FLAGS_RELEASE:STRING=-O3

//Flags used by the Fortran compiler during RELWITHDEBINFO builds.
CMAKE_Fortran_FLAGS_RELWITHDEBINFO:STRING=-O2 -g

//Install path prefix, prepended onto install directories.
CMAKE_INSTALL_PREFIX:PATH=/usr/local

//Path to a program.
CMAKE_LINKER:FILEPATH=/usr/bin/ld

//Path to a program.
CMAKE_MAKE_PROGRAM:FILEPATH=/usr/local_rwth/bin/gmake

//Flags used by the linker during the creation of modules during
// all build types.
CMAKE_MODULE_LINKER_FLAGS:STRING=

//Flags used by the linker during the creation of modules during
// DEBUG builds.
CMAKE_MODULE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during the creation of modules during
// MINSIZEREL builds.
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during the creation of modules during
// RELEASE builds.
CMAKE_MODULE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during the creation of modules during
// RELWITHDEBINFO builds.
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_NM:FILEPATH=/usr/bin/nm

//Path to a program.
CMAKE_OBJCOPY:FILEPATH=/usr/bin/objcopy

//Path to a program.
CMAKE_OBJDUMP:FILEPATH=/usr/bin/objdump

//Value Computed by CMake
CMAKE_PROJECT_DESCRIPTION:STATIC=Shiny

//Value Computed by CMake
CMAKE_PROJECT_HOMEPAGE_URL:STATIC=

//Value Computed by CMake
CMAKE_PROJECT_NAME:STATIC=SHEMAT-Suite

//Value Computed by CMake
CMAKE_PROJECT_VERSION:STATIC=8.40

//Value Computed by CMake
CMAKE_PROJECT_VERSION_MAJOR:STATIC=8

//Value Computed by CMake
CMAKE_PROJECT_VERSION_MINOR:STATIC=40

//Value Computed by CMake
CMAKE_PROJECT_VERSION_PATCH:STATIC=

//Value Computed by CMake
CMAKE_PROJECT_VERSION_TWEAK:STATIC=

//Path to a program.
CMAKE_RANLIB:FILEPATH=/usr/bin/ranlib

//Path to a program.
CMAKE_READELF:FILEPATH=/usr/bin/readelf

//Flags used by the linker during the creation of shared libraries
// during all build types.
CMAKE_SHARED_LINKER_FLAGS:STRING=

//Flags used by the linker during the creation of shared libraries
// during DEBUG builds.
CMAKE_SHARED_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during the creation of shared libraries
// during MINSIZEREL builds.
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during the creation of shared libraries
// during RELEASE builds.
CMAKE_SHARED_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during the creation of shared libraries
// during RELWITHDEBINFO builds.
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//If set, runtime paths are not added when installing shared libraries,
// but are added when building.
CMAKE_SKIP_INSTALL_RPATH:BOOL=NO

//If set, runtime paths are not added when using shared libraries.
CMAKE_SKIP_RPATH:BOOL=NO

//Flags used by the linker during the creation of static libraries
// during all build types.
CMAKE_STATIC_LINKER_FLAGS:STRING=

//Flags used by the linker during the creation of static libraries
// during DEBUG builds.
CMAKE_STATIC_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during the creation of static libraries
// during MINSIZEREL builds.
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during the creation of static libraries
// during RELEASE builds.
CMAKE_STATIC_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during the creation of static libraries
// during RELWITHDEBINFO builds.
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_STRIP:FILEPATH=/usr/bin/strip

//If this value is on, makefiles will be generated without the
// .SILENT directive, and all commands will be echoed to the console
// during the make.  This is useful for debugging only. With Visual
// Studio IDE projects all commands are done without /nologo.
CMAKE_VERBOSE_MAKEFILE:BOOL=FALSE

//HDF5 file differencing tool.
HDF5_DIFF_EXECUTABLE:FILEPATH=/usr/local_rwth/sw/HDF5/1.10.4/intel_19.0.1.144-intelmpi_2018.4.274/bin/h5diff

//The directory containing a CMake configuration file for HDF5.
HDF5_DIR:PATH=HDF5_DIR-NOTFOUND

//HDF5 Fortran Wrapper compiler.  Used only to detect HDF5 compile
// flags.
HDF5_Fortran_COMPILER_EXECUTABLE:FILEPATH=/usr/local_rwth/sw/HDF5/1.10.4/intel_19.0.1.144-intelmpi_2018.4.274/bin/h5pfc

//Path to a library.
HDF5_Fortran_LIBRARY_dl:FILEPATH=/usr/lib64/libdl.so

//Path to a library.
HDF5_Fortran_LIBRARY_hdf5:FILEPATH=/usr/local_rwth/sw/HDF5/1.10.4/intel_19.0.1.144-intelmpi_2018.4.274/lib/libhdf5.so

//Path to a library.
HDF5_Fortran_LIBRARY_hdf5_fortran:FILEPATH=/usr/local_rwth/sw/HDF5/1.10.4/intel_19.0.1.144-intelmpi_2018.4.274/lib/libhdf5_fortran.so

//Path to a library.
HDF5_Fortran_LIBRARY_hdf5_hl:FILEPATH=/usr/local_rwth/sw/HDF5/1.10.4/intel_19.0.1.144-intelmpi_2018.4.274/lib/libhdf5_hl.so

//Path to a library.
HDF5_Fortran_LIBRARY_hdf5hl_fortran:FILEPATH=/usr/local_rwth/sw/HDF5/1.10.4/intel_19.0.1.144-intelmpi_2018.4.274/lib/libhdf5hl_fortran.so

//Path to a library.
HDF5_Fortran_LIBRARY_m:FILEPATH=/usr/lib64/libm.so

//Path to a library.
HDF5_Fortran_LIBRARY_z:FILEPATH=/usr/lib64/libz.so

//Inversion methods
INVTYPE:STRING=STBAY

//Nonlinear Solver
NLSOLVETYPE:STRING=stdFW

//CXX compiler flags for OpenMP parallelization
OpenMP_CXX_FLAGS:STRING=-qopenmp

//CXX compiler libraries for OpenMP parallelization
OpenMP_CXX_LIB_NAMES:STRING=iomp5;pthread

//C compiler flags for OpenMP parallelization
OpenMP_C_FLAGS:STRING=-qopenmp

//C compiler libraries for OpenMP parallelization
OpenMP_C_LIB_NAMES:STRING=iomp5;pthread

//Fortran compiler flags for OpenMP parallelization
OpenMP_Fortran_FLAGS:STRING=-qopenmp

//Fortran compiler libraries for OpenMP parallelization
OpenMP_Fortran_LIB_NAMES:STRING=iomp5

//Path to the iomp5 library for OpenMP
OpenMP_iomp5_LIBRARY:FILEPATH=/rwthfs/rz/SW/intel/Compiler/19.0/1.144/compilers_and_libraries_2019.1.144/linux/compiler/lib/intel64_lin/libiomp5.so

//Path to the pthread library for OpenMP
OpenMP_pthread_LIBRARY:FILEPATH=/usr/lib64/libpthread.so

//The Properties to be used (standard is const)
PROPS:STRING=const

//Value Computed by CMake
SHEMAT-Suite_BINARY_DIR:STATIC=/home/mv897575/SHEMAT-Suite/build_const

//Value Computed by CMake
SHEMAT-Suite_SOURCE_DIR:STATIC=/home/mv897575/SHEMAT-Suite

//Used Simulation (sgsim)
SIMUL:STRING=sgsim

//The Userfunction to be used (standard is none)
USER:STRING=none

//Path to a program.
XIAR:FILEPATH=/opt/intel/Compiler/19.0/1.144/rwthlnk/bin/intel64/xiar

//Compile with debug informations
debug:BOOL=OFF

//Show compilation details
details:BOOL=OFF

//HDF5 switch
hdf:BOOL=ON

//MPI switch
mpi:BOOL=OFF

//OpenMP switch
omp:BOOL=ON

//PLT switch
plt:BOOL=ON

//Use Reverse-Mode in Inversion
use_rm:BOOL=OFF

//VTK switch
vtk:BOOL=ON


########################
# INTERNAL cache entries
########################

//Have Fortran function sgemm
BLAS_WORKS:INTERNAL=
//ADVANCED property for variable: BLAS_guide_LIBRARY
BLAS_guide_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: BLAS_iomp5_LIBRARY
BLAS_iomp5_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: BLAS_mkl_core_LIBRARY
BLAS_mkl_core_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: BLAS_mkl_intel_LIBRARY
BLAS_mkl_intel_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: BLAS_mkl_intel_lp64_LIBRARY
BLAS_mkl_intel_lp64_LIBRARY-ADVANCED:INTERNAL=1
//Have Fortran function sgemm
BLAS_mkl_intel_lp64_thread_mkl_intel_thread_thread_mkl_core_thread_iomp5_thread_WORKS:INTERNAL=1
//ADVANCED property for variable: BLAS_mkl_intel_thread_LIBRARY
BLAS_mkl_intel_thread_LIBRARY-ADVANCED:INTERNAL=1
//Have Fortran function sgemm
BLAS_mkl_intel_thread_mkl_intel_thread_thread_mkl_core_thread_iomp5_thread_WORKS:INTERNAL=
//ADVANCED property for variable: CMAKE_ADDR2LINE
CMAKE_ADDR2LINE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_AR
CMAKE_AR-ADVANCED:INTERNAL=1
//This is the directory where this CMakeCache.txt was created
CMAKE_CACHEFILE_DIR:INTERNAL=/home/mv897575/SHEMAT-Suite/build_const
//Major version of cmake used to create the current loaded cache
CMAKE_CACHE_MAJOR_VERSION:INTERNAL=3
//Minor version of cmake used to create the current loaded cache
CMAKE_CACHE_MINOR_VERSION:INTERNAL=16
//Patch version of cmake used to create the current loaded cache
CMAKE_CACHE_PATCH_VERSION:INTERNAL=4
//ADVANCED property for variable: CMAKE_COLOR_MAKEFILE
CMAKE_COLOR_MAKEFILE-ADVANCED:INTERNAL=1
//Path to CMake executable.
CMAKE_COMMAND:INTERNAL=/rwthfs/rz/SW/UTIL.common/cmake/3.16.4/bin/cmake
//Path to cpack program executable.
CMAKE_CPACK_COMMAND:INTERNAL=/rwthfs/rz/SW/UTIL.common/cmake/3.16.4/bin/cpack
//Path to ctest program executable.
CMAKE_CTEST_COMMAND:INTERNAL=/rwthfs/rz/SW/UTIL.common/cmake/3.16.4/bin/ctest
//ADVANCED property for variable: CMAKE_CXX_COMPILER
CMAKE_CXX_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS
CMAKE_CXX_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_DEBUG
CMAKE_CXX_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_MINSIZEREL
CMAKE_CXX_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELEASE
CMAKE_CXX_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELWITHDEBINFO
CMAKE_CXX_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER
CMAKE_C_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS
CMAKE_C_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_DEBUG
CMAKE_C_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_MINSIZEREL
CMAKE_C_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELEASE
CMAKE_C_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELWITHDEBINFO
CMAKE_C_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_DLLTOOL
CMAKE_DLLTOOL-ADVANCED:INTERNAL=1
//Path to cache edit program executable.
CMAKE_EDIT_COMMAND:INTERNAL=/rwthfs/rz/SW/UTIL.common/cmake/3.16.4/bin/ccmake
//Executable file format
CMAKE_EXECUTABLE_FORMAT:INTERNAL=ELF
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS
CMAKE_EXE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_DEBUG
CMAKE_EXE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_MINSIZEREL
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELEASE
CMAKE_EXE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXPORT_COMPILE_COMMANDS
CMAKE_EXPORT_COMPILE_COMMANDS-ADVANCED:INTERNAL=1
//Name of external makefile project generator.
CMAKE_EXTRA_GENERATOR:INTERNAL=
//ADVANCED property for variable: CMAKE_Fortran_COMPILER
CMAKE_Fortran_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS
CMAKE_Fortran_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS_DEBUG
CMAKE_Fortran_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS_MINSIZEREL
CMAKE_Fortran_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS_RELEASE
CMAKE_Fortran_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS_RELWITHDEBINFO
CMAKE_Fortran_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//Name of generator.
CMAKE_GENERATOR:INTERNAL=Unix Makefiles
//Generator instance identifier.
CMAKE_GENERATOR_INSTANCE:INTERNAL=
//Name of generator platform.
CMAKE_GENERATOR_PLATFORM:INTERNAL=
//Name of generator toolset.
CMAKE_GENERATOR_TOOLSET:INTERNAL=
//Test CMAKE_HAVE_LIBC_PTHREAD
CMAKE_HAVE_LIBC_PTHREAD:INTERNAL=1
//Have include pthread.h
CMAKE_HAVE_PTHREAD_H:INTERNAL=1
//Source directory with the top level CMakeLists.txt file for this
// project
CMAKE_HOME_DIRECTORY:INTERNAL=/home/mv897575/SHEMAT-Suite
//Install .so files without execute permission.
CMAKE_INSTALL_SO_NO_EXE:INTERNAL=0
//ADVANCED property for variable: CMAKE_LINKER
CMAKE_LINKER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MAKE_PROGRAM
CMAKE_MAKE_PROGRAM-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS
CMAKE_MODULE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_DEBUG
CMAKE_MODULE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELEASE
CMAKE_MODULE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_NM
CMAKE_NM-ADVANCED:INTERNAL=1
//number of local generators
CMAKE_NUMBER_OF_MAKEFILES:INTERNAL=1
//ADVANCED property for variable: CMAKE_OBJCOPY
CMAKE_OBJCOPY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_OBJDUMP
CMAKE_OBJDUMP-ADVANCED:INTERNAL=1
//Platform information initialized
CMAKE_PLATFORM_INFO_INITIALIZED:INTERNAL=1
//ADVANCED property for variable: CMAKE_RANLIB
CMAKE_RANLIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_READELF
CMAKE_READELF-ADVANCED:INTERNAL=1
//Path to CMake installation.
CMAKE_ROOT:INTERNAL=/rwthfs/rz/SW/UTIL.common/cmake/3.16.4/share/cmake-3.16
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS
CMAKE_SHARED_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_DEBUG
CMAKE_SHARED_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELEASE
CMAKE_SHARED_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_INSTALL_RPATH
CMAKE_SKIP_INSTALL_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_RPATH
CMAKE_SKIP_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS
CMAKE_STATIC_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_DEBUG
CMAKE_STATIC_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELEASE
CMAKE_STATIC_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STRIP
CMAKE_STRIP-ADVANCED:INTERNAL=1
//uname command
CMAKE_UNAME:INTERNAL=/usr/bin/uname
//ADVANCED property for variable: CMAKE_VERBOSE_MAKEFILE
CMAKE_VERBOSE_MAKEFILE-ADVANCED:INTERNAL=1
//Details about finding BLAS
FIND_PACKAGE_MESSAGE_DETAILS_BLAS:INTERNAL=[/opt/intel/Compiler/19.0/1.144/rwthlnk/mkl/lib/intel64_lin/libmkl_intel_lp64.so;/opt/intel/Compiler/19.0/1.144/rwthlnk/mkl/lib/intel64_lin/libmkl_intel_thread.so;/opt/intel/Compiler/19.0/1.144/rwthlnk/mkl/lib/intel64_lin/libmkl_core.so;/opt/intel/Compiler/19.0/1.144/rwthlnk/compiler/lib/intel64_lin/libiomp5.so;-lm;-ldl][v()]
//Details about finding HDF5
FIND_PACKAGE_MESSAGE_DETAILS_HDF5:INTERNAL=[/usr/local_rwth/sw/HDF5/1.10.4/intel_19.0.1.144-intelmpi_2018.4.274/lib/libhdf5_fortran.so;/usr/local_rwth/sw/HDF5/1.10.4/intel_19.0.1.144-intelmpi_2018.4.274/lib/libhdf5.so;/usr/lib64/libz.so;/usr/lib64/libdl.so;/usr/lib64/libm.so][/usr/local_rwth/sw/HDF5/1.10.4/intel_19.0.1.144-intelmpi_2018.4.274/lib/libhdf5hl_fortran.so;/usr/local_rwth/sw/HDF5/1.10.4/intel_19.0.1.144-intelmpi_2018.4.274/lib/libhdf5_hl.so][cfound components: Fortran ][v1.10.4()]
//Details about finding OpenMP
FIND_PACKAGE_MESSAGE_DETAILS_OpenMP:INTERNAL=[TRUE][TRUE][TRUE][c ][v5.0()]
//Details about finding OpenMP_C
FIND_PACKAGE_MESSAGE_DETAILS_OpenMP_C:INTERNAL=[-qopenmp][/rwthfs/rz/SW/intel/Compiler/19.0/1.144/compilers_and_libraries_2019.1.144/linux/compiler/lib/intel64_lin/libiomp5.so][/usr/lib64/libpthread.so][v5.0()]
//Details about finding OpenMP_CXX
FIND_PACKAGE_MESSAGE_DETAILS_OpenMP_CXX:INTERNAL=[-qopenmp][/rwthfs/rz/SW/intel/Compiler/19.0/1.144/compilers_and_libraries_2019.1.144/linux/compiler/lib/intel64_lin/libiomp5.so][/usr/lib64/libpthread.so][v5.0()]
//Details about finding OpenMP_Fortran
FIND_PACKAGE_MESSAGE_DETAILS_OpenMP_Fortran:INTERNAL=[-qopenmp][/rwthfs/rz/SW/intel/Compiler/19.0/1.144/compilers_and_libraries_2019.1.144/linux/compiler/lib/intel64_lin/libiomp5.so][v5.0()]
//Details about finding Threads
FIND_PACKAGE_MESSAGE_DETAILS_Threads:INTERNAL=[TRUE][v()]
//ADVANCED property for variable: HDF5_DIFF_EXECUTABLE
HDF5_DIFF_EXECUTABLE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: HDF5_DIR
HDF5_DIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: HDF5_Fortran_COMPILER_EXECUTABLE
HDF5_Fortran_COMPILER_EXECUTABLE-ADVANCED:INTERNAL=1
//Result of TRY_COMPILE
HDF5_Fortran_COMPILER_NO_INTERROGATE:INTERNAL=FALSE
//Have Fortran function cheev
LAPACK_WORKS:INTERNAL=1
//STRINGS property for variable: NLSOLVETYPE
NLSOLVETYPE-STRINGS:INTERNAL=stdFW;nwtFW;nitFW
//Result of TRY_COMPILE
OpenMP_COMPILE_RESULT_CXX_qopenmp:INTERNAL=TRUE
//Result of TRY_COMPILE
OpenMP_COMPILE_RESULT_C_qopenmp:INTERNAL=TRUE
//Result of TRY_COMPILE
OpenMP_COMPILE_RESULT_FortranHeader_qopenmp:INTERNAL=TRUE
//Result of TRY_COMPILE
OpenMP_COMPILE_RESULT_FortranModule_qopenmp:INTERNAL=TRUE
//ADVANCED property for variable: OpenMP_CXX_FLAGS
OpenMP_CXX_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: OpenMP_CXX_LIB_NAMES
OpenMP_CXX_LIB_NAMES-ADVANCED:INTERNAL=1
//CXX compiler's OpenMP specification date
OpenMP_CXX_SPEC_DATE:INTERNAL=201611
//ADVANCED property for variable: OpenMP_C_FLAGS
OpenMP_C_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: OpenMP_C_LIB_NAMES
OpenMP_C_LIB_NAMES-ADVANCED:INTERNAL=1
//C compiler's OpenMP specification date
OpenMP_C_SPEC_DATE:INTERNAL=201611
//ADVANCED property for variable: OpenMP_Fortran_FLAGS
OpenMP_Fortran_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: OpenMP_Fortran_LIB_NAMES
OpenMP_Fortran_LIB_NAMES-ADVANCED:INTERNAL=1
//Fortran compiler's OpenMP specification date
OpenMP_Fortran_SPEC_DATE:INTERNAL=201611
//Result of TRY_COMPILE
OpenMP_SPECTEST_CXX_:INTERNAL=TRUE
//Result of TRY_COMPILE
OpenMP_SPECTEST_C_:INTERNAL=TRUE
//Result of TRY_COMPILE
OpenMP_SPECTEST_Fortran_:INTERNAL=TRUE
//ADVANCED property for variable: OpenMP_iomp5_LIBRARY
OpenMP_iomp5_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: OpenMP_pthread_LIBRARY
OpenMP_pthread_LIBRARY-ADVANCED:INTERNAL=1
//STRINGS property for variable: PROPS
PROPS-STRINGS:INTERNAL=bas;basc;const;ghe;ice
//STRINGS property for variable: USER
USER-STRINGS:INTERNAL=none;wells3d
//ADVANCED property for variable: XIAR
XIAR-ADVANCED:INTERNAL=1
//STRINGS property for variable: phys_base
phys_base-STRINGS:INTERNAL=head;pres
//The used physical pressure representation, could be head or pres
phys_base:INTERNAL=head

