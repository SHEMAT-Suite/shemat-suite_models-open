# Model info
Testmodel for deterministic inversion (AD mode)

* 2D flow and heat transport model
* geology simplified from Perth basin
* layered structure with permeable fault 
* Temperature data are inverted to determine permeability of unit 5
* model by Jan Niederau
* contact person: Johanna Bruckmann

See file Documentation_od_Perth_inputs.doc for more information.

readme_old.md is the description before 09.03.2020.

# Compilation Information
```
module_ad_ifort_const_none_head_master-all.inc
CMakeCache_ad_ifort_const_none_head_master-all.txt
version_ad_ifort_const_none_head_master-all.inc
```

# Execution
```
./shem_ad_ifort_const_none_head_master-all.x
```
