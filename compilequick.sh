#!/bin/zsh

# Compile SHEMAT-Suite executable with CMake and copy it to model_dir

#------------------------------------------------------------------
#-----------------------Variables ---------------------------------
#------------------------------------------------------------------

# "${HOME}/SHEMAT-Suite_Models/fw_const_Example"
model_dir="${HOME}/SHEMAT-Suite_Models/fw_const_Theis_analytical_comparison/"
# "${HOME}/SHEMAT-Suite"
make_dir="${HOME}/SHEMAT-Suite"

shem_type="fw"			# "fw", "ad", "sm"

mode="head"			# "head", "pres"

props="const"			# "const", "bas", "basc", "ghe", "ice"
user="none"			# "none", "wells3d"

compiler_name="gfortran"	       # "gfortran", "ifort"

flag_omp="OFF"			# omp: ON/OFF
flag_hdf="ON"			# hdf: ON/OFF
flag_vtk="OFF"			# vtk: ON/OFF
flag_plt="OFF"			# plt: ON/OFF

flags=""			# Additional Flags

# Directory existence checks
if [ ! -d ${make_dir} ]
then
    echo "   Makefile directory"
    echo ${make_dir}
    echo "   does not exist."
    exit 1
fi
if [ ! -d ${model_dir} ]
then
    echo "   Model directory"
    echo ${model_dir}
    echo "   does not exist."
    exit 1
fi

# Go to make_dir
pushd ${make_dir}

# Get git branch name
git_branch=$(git rev-parse --abbrev-ref HEAD)

# Check source code for changes
git diff --exit-code --quiet
if [ $? -ge 1 ];
then
    echo "   Unstaged changes in Git repository! Please remove."
    echo "   Branch: ${git_branch}"
    exit 1
fi

git diff --cached --exit-code --quiet
if [ $? -ge 1 ];
then
    echo "   Staged changes in Git repository! Please remove."
    echo "   Branch: ${git_branch}"
    exit 1
fi

# Remove old build-directory if it still exists
if [ -d build_${props} ]
then
    rm -r build_${props}
fi

# Go to build-directory
mkdir build_${props}
pushd build_${props}

# CMake command
FC=${compiler_name} cmake -DPROPS=${props} -DUSER=${user} -Dphys_base=${mode} -Domp=${flag_omp} -Dhdf=${flag_hdf} -Dvtk=${flag_vtk} -Dplt=${flag_plt} "${flags}" ..

# Catch CMake errors
if [ $? -ge 1 ];
then
    echo ""
    echo "compilequick.sh: Command:"
    echo "------------------------------------"
    echo "FC=${compiler_name} cmake -DPROPS=${props} -DUSER=${user} -Dphys_base=${mode} ${flags} .."
    echo "------------------------------------"
    echo "compilequick.sh: Compilation aborted"
    popd
    exit 1
fi

# GMake compilation command
gmake ${shem_type} -j16

# Catch GMake errors
if [ $? -ge 1 ];
then
    echo ""
    echo "compilequick.sh: Command:"
    echo "------------------------------------"
    echo "gmake ${shem_type} -j16"
    echo "------------------------------------"
    echo "compilequick.sh: Compilation aborted"
    popd
    exit 1
fi

# Check compiler
if [ ! ${compiler_name} = $(grep "CMAKE_Fortran_COMPILER:FILEPATH" CMakeCache.txt | grep -Eo "[a-zA-Z0-9]+$") ]
then
    echo ""
    echo "compilequick.sh:"
    echo "------------------------------------"
    echo "${compiler_name}"
    echo "not equal to"
    echo "$(grep "CMAKE_Fortran_COMPILER:FILEPATH" CMakeCache.txt | grep -Eo "[a-zA-Z0-9]+$") (compiler_name in CMakeCache.txt)"
    echo "------------------------------------"
    echo "compilequick.sh: Compilation aborted"
    popd
    exit 1
fi

# Executable suffix
new_exe_suffix="${shem_type}_${compiler_name}_${props}_${user}_${mode}_${git_branch}_quick"

# Rename executable
mv shem_${shem_type}_${props}.x shem_${new_exe_suffix}.x

# Copy executable to model_dir
cp shem_${new_exe_suffix}.x ${model_dir}

# Copy CMakeCache.txt, version.inc
cp CMakeCache.txt ${model_dir}
pushd generated
cp version.inc ${model_dir}
popd

# Rename CMakeCache.txt, version.inc
pushd ${model_dir}
mv CMakeCache.txt CMakeCache_${new_exe_suffix}.txt
mv version.inc version_${new_exe_suffix}.inc

# File with module or ldd configuration
if ! type "module" > /dev/null;
then
    ldd shem_${new_exe_suffix}.x > module_${new_exe_suffix}.inc
else
    module list -t 2> module_${new_exe_suffix}.inc
fi

# Clean SHEMAT-Suite git repository
popd
git clean -f

# Remove build-directory
popd
rm -r build_${props}

# Echo paths
echo "--------------------------------------------------------"
echo "Compilation path:"
pwd

popd

echo
echo "Current path:"
pwd

echo
