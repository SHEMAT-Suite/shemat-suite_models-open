# Model Info
Three simple heat conduction models, with an analytical comparison. 

A description of the models and its analytical solutions can be found in `IES-Benchmark.pdf`. Sketches of the models can also be found there.

`SHEMAT_doc/uindex.ipy` creates `SHEMAT_doc/uindex.txt`, which contains the values for `# uindex` of model 2.

# Compilation Information
```
SHEMAT_doc/CMakeCache_fw_ifort_const_none_head_master-all_quick.txt
SHEMAT_doc/version_fw_ifort_const_none_head_master-all_quick.inc
SHEMAT_doc/module_fw_ifort_const_none_head_master-all_quick.inc
```

# Execution
```
./shem_fw_ifort_const_none_head_master-all_quick.x > out.txt
```

# Comparison to analytical solution
The analytical solution and the comparison can be calculated with the given scripts. I tried to create the comparison plots very similar to the ones in the paper.

## Model 1: IES1
Script: `Model_1_IES1_analytical.ipy.ipy`

Plots: `Model_1_Tempreature_Profile.png`, `Model_1_Absolute_Difference.png`, `Model_1_Relative_Difference.png`

Chapter in paper: 2 Two_Block Temperature Model No. 1

The maximum relative difference to the analytical solution is less thann 0.5 %.

## Model 2: IES2
Script: `Model_2_IES2_analytical.ipy.ipy`

Plots: `Model_2_Tempreature_Profile.png`, `Model_2_Absolute_Difference.png`, `Model_2_Relative_Difference.png`

Chapter in paper: 3 Two_Block Temperature Model No. 2

The maximum relative difference to the analytical solution is less thann 0.5 %. 

## Model 3: IES3
Script: `Model_3_IES3_analytical.ipy` The analytical solution results differ from the results given at the end of the paper in the third decimal place, I dont understand why.

Plots: `Model_3_absolute_Differences.png`, `Model_3_relative_Differences.png`, `Model_3_TimevsTemp_at_different_depths.png`, `Model_3_Vertical_Temp_Profile.png`

Chapter in paper: 4 Non Steady State Temperature Model No.1

The maximum relative difference to the analytical solution is less than 1.0 % for the time steps used. By increasing the number of time discretisation steps the differences could be reduced a little bit more, but due to the computing time only 500 time steps are used. By using 5000 time steps the relative deviation from the analytical solution is decreased by about 0.1 percentage points.
