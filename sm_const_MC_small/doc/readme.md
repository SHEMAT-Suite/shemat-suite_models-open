# Model input files:

Synres1                 -       Main input file for SHEMAT-Suite
sgsimk.par              -       Input file for SGSim algorithm which is needed for Monte Carlo simulations
logk_normal.dat         -       Data file which is needed for SGSim algorithm
shemade.job             -       Control file for SHEMAT-Suite containing the name of model input files which will be simulated

# How to Execute on CLAIX-18, 2:

New executable:
```
./shem_sm_sgsim64int_const_head_master.x
```
# Additional Information:
2-dimensional, steady state model which solves the flow equation.
Computes four random realizations of the permeability field.
The number of realizations to be computed is defined in the input file Synres1 with the keyword # simulate.

The file submit.job provides an exemplary CLAIX-18 batch script for this model.

Example for the Monte Carlo simulation (sm mode).


* Total runtime: around 80 seconds


* contact person: Johanna Bruckmann
* date: April 2019

# Compilation Information
```
Makefile_sm_sgsim64int_const_head_master.flags
module_sm_sgsim64int_const_head_master.inc
version_sm_sgsim64int_const_head_master.inc
```
