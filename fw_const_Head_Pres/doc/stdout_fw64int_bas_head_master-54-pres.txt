  
 ======================================
 Shem-FW/AD/SM 7.50 (Smarty)                                                    
  
 ======================================
  
  
  
  *** NEW MODEL 
 
   checking input definitions:
     from file "h_init"
  
   reading model input parameter:
     from file "h_init"
  
  [R] : title
  [R] : runmode
     >>> forward modeling
  <D> : free=0, confined flow !
  [R] : linfo
  [R] : command line OpenMP thread configuration (  1x  1 threads)
  [R] : [i0, j0, k0] = [  50,   1,  50]
  [R] : nonlinear solver parameter
  [I] : fixed relxation factor assumed
  [R] : output suffix: .h5:[X], .plt:[X], .vtk:[X], .txt:[ ]
  
   reading flow parameters
  
  [R] : flow linear solver (control=  67)
  [R] : flow nonlinear iteration tolerance
  [I] : flow nonlinear relaxation factor
  
  <D> : no temperature [disabled]
  
  
  <D> : no transport [disabled]
  
  
  <D> : no electric potential [disabled]
  
  
  <D> : grav =   0.9810E+01
  <D> : no fluid heat production
  <D> : rref, reference density =   0.9980E+03
  <D> : tref, reference temperature =   0.2000E+02
  <D> : fixed rhocm, defined by unit
  
   reading zone parameter 
  
  [R] : "uindex" - unit index number, unit-cell assignment
  [I] : approx. speedup for the full solver :   1.00
  
  [R] : "units" - unit (rock) properties
  
   reading/preprocessing arrays
  
  [R] : delx
  [R] : dely
  [R] : delz
  
   reading initial values 
  
  [R] : head, in [m]
  [R] : temp, in [degree Celsius]
  [R] : pres, in [MPa]
  
   reading boundary conditions 
  
  [R] : head bcd, simple=top  , (size=    50), bcmy=  0.1000E+19, error=ignore, (offset=     1)
  [R] : head bcn, simple=base , (size=    50), bcmy=  0.1000E+19, error=ignore, (offset=    51)
  
  <D> : property limitations
  [R] : No default Darcy velocity - head active!
  
   reading external file control
  
 
   reading time input parameter:
     from file "h_init"
 
  <D> : Steady state !
  <D> : tunit =          0.10000000E+01 !
  <D> : tstart=          0.00000000E+00
  <D> : tshift=          0.00000000E+00
  <D> : titer =      0
  <D> : no monitoring points !
  <D> : no output times !
 
 
  [W] : "h_init_status.log"
 
  [I] : memory:       1.734 MByte data
 
  nrm2(R) = 0.2696434733698D-25 (direct)
  [I] : iter_nl =     1, difmaxh =  0.99279565E+00, difmaxt =  0.10000000E-20
  nrm2(R) = 0.2772922277741D-25 (direct)
  [I] : iter_nl =     2, difmaxh =  0.11487305E-02, difmaxt =  0.10000000E-20
  nrm2(R) = 0.2783818989063D-25 (direct)
  [I] : iter_nl =     3, difmaxh =  0.94723185E-06, difmaxt =  0.10000000E-20
  nrm2(R) = 0.2751292896597D-25 (direct)
  [I] : iter_nl =     4, difmaxh =  0.60504135E-09, difmaxt =  0.10000000E-20
  nrm2(R) = 0.2758314285072D-25 (direct)
  [I] : iter_nl =     5, difmaxh = -0.39904080E-10, difmaxt =  0.10000000E-20
  [W] : HDF5 to "h_init_final.h5"
  [W] : Tecplot to "h_init_final.plt"
  [W] : VTK to "h_init_final.vtk"
  [W] : Borehole logs to "h_init_final_L1.dat"
 Deallocated Memory, remaining   7.629394531250000E-006  MB
  
  
  *** NEW MODEL 
 
   checking input definitions:
     from file "t_init"
  
   reading model input parameter:
     from file "t_init"
  
  [R] : title
  [R] : runmode
     >>> forward modeling
  <D> : free=0, confined flow !
  [R] : linfo
  [R] : command line OpenMP thread configuration (  1x  1 threads)
  [R] : [i0, j0, k0] = [  50,   1,  50]
  [R] : nonlinear solver parameter
  [I] : fixed relxation factor assumed
  [R] : output suffix: .h5:[X], .plt:[X], .vtk:[X], .txt:[ ]
  
  <D> : no flow [disabled]
  
  
   reading heat transport parameters
  
  [R] : temperature linear solver (control=  67)
  [R] : temperature nonlinear iteration tolerance
  [I] : temperature nonlinear relaxation factor
  
  <D> : no transport [disabled]
  
  
  <D> : no electric potential [disabled]
  
  
  <D> : grav =   0.9810E+01
  <D> : no fluid heat production
  <D> : rref, reference density =   0.9980E+03
  <D> : tref, reference temperature =   0.2000E+02
  <D> : fixed rhocm, defined by unit
  
   reading zone parameter 
  
  [R] : "uindex" - unit index number, unit-cell assignment
  [I] : approx. speedup for the full solver :   1.00
  
  [R] : "units" - unit (rock) properties
  
   reading/preprocessing arrays
  
  [R] : delx
  [R] : dely
  [R] : delz
  
   reading initial values 
  
  [R] : head, in [m]
  [R] : temp, in [degree Celsius]
  [R] : pres, in [MPa]
  
   reading boundary conditions 
  
  [R] : temp bcd, simple=top  , (size=    50), bcmy=  0.1000E+19, error=ignore, (offset=     1)
  [R] : temp bcn, simple=base , (size=    50), bcmy=  0.1000E+19, error=ignore, (offset=    51)
  
  <D> : property limitations
  [R] : Zero default Darcy velocity -  no # velocity!
  
   reading external file control
  
 
   reading time input parameter:
     from file "t_init"
 
  <D> : Steady state !
  <D> : tunit =          0.10000000E+01 !
  <D> : tstart=          0.00000000E+00
  <D> : tshift=          0.00000000E+00
  <D> : titer =      0
  <D> : no monitoring points !
  <D> : no output times !
 
 
  [W] : "t_init_status.log"
 
  [I] : memory:       1.734 MByte data
 
  nrm2(R) = 0.2391025109621D-13 (direct)
  [I] : iter_nl =     1, difmaxh =  0.10000000E-20, difmaxt = -0.98216435E+01
  nrm2(R) = 0.2350697968722D-13 (direct)
  [I] : iter_nl =     2, difmaxh =  0.10000000E-20, difmaxt = -0.55833498E-01
  nrm2(R) = 0.2350019050578D-13 (direct)
  [I] : iter_nl =     3, difmaxh =  0.10000000E-20, difmaxt = -0.35304858E-03
  nrm2(R) = 0.2406721085279D-13 (direct)
  [I] : iter_nl =     4, difmaxh =  0.10000000E-20, difmaxt = -0.13026821E-05
  nrm2(R) = 0.2360445991722D-13 (direct)
  [I] : iter_nl =     5, difmaxh =  0.10000000E-20, difmaxt = -0.34513477E-08
  nrm2(R) = 0.2368000687152D-13 (direct)
  [I] : iter_nl =     6, difmaxh =  0.10000000E-20, difmaxt = -0.77982065E-11
  [W] : HDF5 to "t_init_final.h5"
  [W] : Tecplot to "t_init_final.plt"
  [W] : VTK to "t_init_final.vtk"
  [W] : Borehole logs to "t_init_final_L1.dat"
 Deallocated Memory, remaining   7.629394531250000E-006  MB
  
  
  *** NEW MODEL 
 
   checking input definitions:
     from file "ht_init"
  
   reading model input parameter:
     from file "ht_init"
  
  [R] : title
  [R] : runmode
     >>> forward modeling
  <D> : free=0, confined flow !
  [R] : linfo
  [R] : command line OpenMP thread configuration (  1x  1 threads)
  [R] : [i0, j0, k0] = [  50,   1,  50]
  [R] : nonlinear solver parameter
  [I] : fixed relxation factor assumed
  [R] : output suffix: .h5:[X], .plt:[X], .vtk:[X], .txt:[ ]
  
   reading flow parameters
  
  [R] : flow linear solver (control=  67)
  [R] : flow nonlinear iteration tolerance
  [I] : flow nonlinear relaxation factor
  
   reading heat transport parameters
  
  [R] : temperature linear solver (control=  67)
  [R] : temperature nonlinear iteration tolerance
  [I] : temperature nonlinear relaxation factor
  
  <D> : no transport [disabled]
  
  
  <D> : no electric potential [disabled]
  
  
  <D> : grav =   0.9810E+01
  <D> : no fluid heat production
  <D> : rref, reference density =   0.9980E+03
  <D> : tref, reference temperature =   0.2000E+02
  <D> : fixed rhocm, defined by unit
  
   reading zone parameter 
  
  [R] : "uindex" - unit index number, unit-cell assignment
  [I] : approx. speedup for the full solver :   1.00
  
  [R] : "units" - unit (rock) properties
  
   reading/preprocessing arrays
  
  [R] : delx
  [R] : dely
  [R] : delz
  
   reading initial values 
  
   open HDF5 file: h_init_final.h5
  [R] : head, in [m]
   open HDF5 file: h_init_final.h5
  [R] : temp, in [degree Celsius]
  [R] : pres, in [MPa]
  
   reading boundary conditions 
  
  [R] : head bcd, simple=top  , (size=    50), bcmy=  0.1000E+19, error=ignore, (offset=     1)
  [R] : head bcn, simple=base , (size=    50), bcmy=  0.1000E+19, error=ignore, (offset=    51)
  [R] : temp bcd, simple=top  , (size=    50), bcmy=  0.1000E+19, error=ignore, (offset=   101)
  [R] : temp bcn, simple=base , (size=    50), bcmy=  0.1000E+19, error=ignore, (offset=   151)
  
  <D> : property limitations
  [R] : No default Darcy velocity - head active!
  
   reading external file control
  
 
   reading time input parameter:
     from file "ht_init"
 
  <D> : Steady state !
  <D> : tunit =          0.10000000E+01 !
  <D> : tstart=          0.00000000E+00
  <D> : tshift=          0.00000000E+00
  <D> : titer =      0
  <D> : no monitoring points !
  <D> : no output times !
 
 
  [W] : "ht_init_status.log"
 
  [I] : memory:       1.744 MByte data
 
  nrm2(R) = 0.2762186631783D-25 (direct)
  nrm2(R) = 0.2364653376923D-13 (direct)
  [I] : iter_nl =     1, difmaxh =  0.52637006E-10, difmaxt = -0.54611217E-06
  nrm2(R) = 0.2765214032000D-25 (direct)
  nrm2(R) = 0.2370107712847D-13 (direct)
  [I] : iter_nl =     2, difmaxh =  0.21346978E-08, difmaxt = -0.26035707E-09
  nrm2(R) = 0.2725729152375D-25 (direct)
  nrm2(R) = 0.2325263597324D-13 (direct)
  [I] : iter_nl =     3, difmaxh =  0.34958703E-10, difmaxt =  0.75495166E-12
  [W] : HDF5 to "ht_init_final.h5"
  [W] : Tecplot to "ht_init_final.plt"
  [W] : VTK to "ht_init_final.vtk"
  [W] : Borehole logs to "ht_init_final_L1.dat"
 Deallocated Memory, remaining   7.629394531250000E-006  MB
 total cpu time:    0: 0.81 min
 RUN O.K.
