# import matplotlib.pyplot as plt
import numpy as np
import os
# import seaborn as sb

try:
    import pskf.tools.plot.plotfunctions as pf
except ImportError:
    raise ImportError(
        'No module named pskf.tools.plot.plotfunctions. \n\n' + 'Solution: ' +
        'Download pyshemkf from https://github.com/jjokella/pyshemkf and add' +
        ' the path /<path-to-pyshemkf>/pyshemkf/site-packages to the' +
        ' PYTHONPATH.')

# ht_init results
ht_head = pf.my_hdf(
    os.environ['HOME'] + '/SHEMAT-Suite_Models/fw_const_Head_Pres/' +
    'result/'+ 'ht_init_final.h5', 'head')
ht_pres = pf.my_hdf(
    os.environ['HOME'] + '/SHEMAT-Suite_Models/fw_const_Head_Pres/' +
    'result/'+ 'ht_init_final.h5', 'pres')
ht_vx = pf.my_hdf(
    os.environ['HOME'] + '/SHEMAT-Suite_Models/fw_const_Head_Pres/' +
    'result/'+ 'ht_init_final.h5', 'vx')
ht_temp = pf.my_hdf(
    os.environ['HOME'] + '/SHEMAT-Suite_Models/fw_const_Head_Pres/' +
    'result/'+ 'ht_init_final.h5', 'temp')

# # Coordinates
# x = pf.my_hdf(
#     os.environ['HOME'] + '/SHEMAT-Suite_Models/fw_const_Head_Pres/' +
#     'result/'+ 'ht_init_final.h5', 'x')
# y = pf.my_hdf(
#     os.environ['HOME'] + '/SHEMAT-Suite_Models/fw_const_Head_Pres/' +
#     'result/'+ 'ht_init_final.h5', 'y')
z = pf.my_hdf(
    os.environ['HOME'] + '/SHEMAT-Suite_Models/fw_const_Head_Pres/' +
    'result/'+ 'ht_init_final.h5', 'z')

# pt_init results
pt_pres = pf.my_hdf(
    os.environ['HOME'] + '/SHEMAT-Suite_Models/fw_pres_const_Head_Pres/' +
    'result/'+ 'pt_init_final.h5', 'pres')
pt_head = pf.my_hdf(
    os.environ['HOME'] + '/SHEMAT-Suite_Models/fw_pres_const_Head_Pres/' +
    'result/'+ 'pt_init_final.h5', 'head')
pt_vx = pf.my_hdf(
    os.environ['HOME'] + '/SHEMAT-Suite_Models/fw_pres_const_Head_Pres/' +
    'result/'+ 'pt_init_final.h5', 'vx')
pt_temp = pf.my_hdf(
    os.environ['HOME'] + '/SHEMAT-Suite_Models/fw_pres_const_Head_Pres/' +
    'result/'+ 'pt_init_final.h5', 'temp')

# Conversion via Python
# ---------------------

# Input parameters
psurf = 1.0 * 10**5
rho0 = 998.0
g = 9.81

pres_ana = rho0 * g * (ht_head - z) / 10**6 + psurf / 10**6

head_ana = (pt_pres - psurf / 10**6) / (rho0 * g) * 10**6 + z

# Analytical temperature solution
# -------------------------------

# Input parameters

# Porosity
por = 0.15

# Thermal conductivities W/Km
lamm = 2.4
lamf = 0.65

lam = lamf**por * lamm**(1 - por)

# Top temperature degC
temp_bcd_top = 10.0

# Base heat flow [q] = W/m2
temp_bcn_base = 0.035

# From: q = lam * gradtemp
gradtemp = temp_bcn_base / lam

# Height of the model
h = z[0, 0, -1]

# From T = T_top - (h-z)*gradtemp
temp_ana = temp_bcd_top + (h - z[0, 0, :]) * gradtemp

# Maximal differences
# -------------------
print('\n')
print('Maximal Head differences (ht_init/pt_init):')
print('(1) ht_init-head vs pt_init-head')
print('(2) ht_init-head vs Python-head from pt_init-pressure')
print('(3) pt_init-head vs Python-head from pt_init-pressure')
# Head from head/pres
print(np.max(np.abs(ht_head - pt_head)))

# Head from head/python
print(np.max(np.abs(ht_head - head_ana)))

# Head from pres/python
print(np.max(np.abs(pt_head - head_ana)))

print('\n')
print('Maximal Pressure differences (ht_init/pt_init):')
print('(1) ht_init-pressure vs pt_init-pressure')
print('(2) ht_init-pressure vs Python-pressure from ht_init-head')
print('(3) pt_init-pressure vs Python-pressure from ht_init-head')
# Pres from head/pres
print(np.max(np.abs(ht_pres - pt_pres)))

# Pres from head/python
print(np.max(np.abs(ht_pres - pres_ana)))

# Pres from pres/python
print(np.max(np.abs(pt_pres - pres_ana)))

print('\n')
print('Maximal Temp differences (ht_init-temp/pt_init-temp)):')
print('(1) ht_init-temp vs pt_init-temp')
print('(2) ht_init-temp vs Python-temp')
print('(3) pt_init-temp vs Python-temp')
# Temp from head/pres
print(np.max(np.abs(ht_temp - pt_temp)))

# Temp from head/python
print(np.max(np.abs(ht_temp - temp_ana)))

# Temp from pres/python
print(np.max(np.abs(pt_temp - temp_ana)))
